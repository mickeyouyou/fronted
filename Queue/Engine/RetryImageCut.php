<?php
namespace Queue\Engine;
use Queue\Engine;

class ImageCut extends Engine
{
    function execute($params)
    {
        try {
        	$imgMaxNum = 50;		//图片最大数
        	//创建生成的文件路径
        	$product_id = $params['product_id'];
        	$tmp_path = ABSPATH."uploads/temp/tmp_detail/";
        	
        	//处理生成jpg文件名称
        	if(strpos($params['tmp_filename'], "/")){
        		$arr = explode("/",$params['tmp_filename']);
        		$arr_jpg = explode(".",$arr[1]);
        		$create_name = $arr_jpg[0];
        	}else{
        		$arr_jpg = explode(".",$params['tmp_filename']);
        		$create_name = $arr_jpg[0];
        	}
        	
        	//获取html文件内容
        	$html_content = file_get_contents($tmp_path.$create_name.".html");
        	
        	//只截取50个
        	$pattern = '#<img[^>]+/?>#Uis';
        	if (($imgNum = preg_match_all($pattern, $html_content, $result)) > $imgMaxNum){
        		$lastImage  = $result[0][$imgMaxNum - 1];
        		$position   = strpos($html_content, $lastImage) + strlen($lastImage);
        		$subContent = substr($html_content, 0, $position);
        		
        		$new_tmp_filename = "./uploads/temp/tmp_detail/";
        		$meta_str = '<meta http-equiv="Content-Type" content-type="text/html" charset="UTF-8" />';
        		$style_str = "<style>body{width:790px;background: #fff;}</style>";
        		$body_start = "<body><div style='margin:0 auto;width:100%;'>";
        		$body_end = "</div></body>";
        			
        		file_put_contents($tmp_path.$create_name.'_retry.html', $meta_str.$style_str.$body_start.$subContent.$body_end);
        	}else{
        		$imgMaxNum = $imgNum;		//图片自身数量
        		$subContent = $html_content;
        			
        		file_put_contents($tmp_path.$create_name.'_retry.html',$subContent);
        	}
        	
        		        	
        	//生成图片
        	if(!file_exists(ABSPATH."uploads/products/".$product_id."/img")){
        		mkdir(ABSPATH."uploads/products/".$product_id."/img",0777,true);
        	}
        	
        	//①html转图片
        	$src_img_path = ABSPATH."uploads/products/".$product_id."/".$create_name.'.jpg';
		    wkhtmltox_convert(
	        	'image',array(
	        	'screenWidth' => 810,
	        	//'quality' => 95,
	        	'out' => $src_img_path,
	        	'in'  => $tmp_path.$create_name.'_retry.html',
	        	)
        	);
		    
			while(filesize($src_img_path) > 1024*1024*5) {
				$imgMaxNum -= 5;
				
				//获取html文件内容
				$html_content = file_get_contents($tmp_path.$create_name.".html");
				 
				$pattern = '#<img[^>]+/?>#Uis';
				if (($imgNum = preg_match_all($pattern, $html_content, $result)) > $imgMaxNum){
					$lastImage  = $result[0][$imgMaxNum - 1];
					$position   = strpos($html_content, $lastImage) + strlen($lastImage);
					$subContent = substr($html_content, 0, $position);
				
					$new_tmp_filename = "./uploads/temp/tmp_detail/";
					$meta_str = '<meta http-equiv="Content-Type" content-type="text/html" charset="UTF-8" />';
					$style_str = "<style>body{width:790px;background: #fff;}</style>";
					$body_start = "<body><div style='margin:0 auto;width:100%;'>";
					$body_end = "</div></body>";
					 
					file_put_contents($tmp_path.$create_name.'_retry.html', $meta_str.$style_str.$body_start.$subContent.$body_end);
				}else{
					$subContent = $html_content;
					 
					file_put_contents($tmp_path.$create_name.'_retry.html',$subContent);
				}
				 
					
				//生成图片
				if(!file_exists(ABSPATH."uploads/products/".$product_id."/img")){
					mkdir(ABSPATH."uploads/products/".$product_id."/img",0777,true);
				}
				 
				//①html转图片
				$src_img_path = ABSPATH."uploads/products/".$product_id."/".$create_name.'.jpg';
				wkhtmltox_convert(
					'image',array(
							'screenWidth' => 810,
							//'quality' => 95,
							'out' => $src_img_path,
							'in'  => $tmp_path.$create_name.'_retry.html',
					)
				);
				
				clearstatcache();	//清空文件缓存
			}
	        
        	//②切割图片
        	$link= $src_img_path;	//图片路径
        	$img = imagecreatefromjpeg($link);
        	
        	$maxW=imagesx($img);
        	$maxH = (imagesy($img)/50);
        	list($width, $height, $type, $attr) = getimagesize($link);
        	$widthnum=ceil($width/$maxW);
        	$heightnum=ceil($height/$maxH);
        	$iOut = imagecreatetruecolor ($maxW,$maxH);
        	//bool imagecopy ( resource dst_im, resource src_im, int dst_x, int dst_y, int src_x, int src_y, int src_w, int src_h )
        	//将 src_im 图像中坐标从 src_x，src_y 开始，宽度为 src_w，高度为 src_h 的一部分拷贝到 dst_im 图像中坐标为 dst_x 和 dst_y 的位置上。
        	
        	$x_small_pic = ABSPATH."uploads/products/".$product_id."/img/";   //存放小图地址
        	if(!file_exists($x_small_pic))
        		mkdir($x_small_pic,0777,true);
        	
        	if(getimagesize($link) !== false){
        		for ($i=0;$i < $heightnum;$i++) {
        			for ($j=0;$j < $widthnum;$j++) {
        				imagecopy($iOut,$img,0,0,($j*$maxW),($i*$maxH),$maxW,$maxH);//复制图片的一部分
        				imagejpeg($iOut,$x_small_pic.$i.".jpg"); //输出成0.jpg,1.jpg这样的格式
        			}
        		}
        	}
        	
        	return true;
        	
        } catch (Exception $e) {
        	
            echo ($e->getMessage() . "\n" .
                   "Errno: ". $e->getCode() . "\n" .
                   "Params: " . var_export($params, true) . "\n" .
                   "In " . $e->getFile() . "\n" .
                   "On Line " . $e->getLine());
            return 0;
        }
    }
}