<?php
namespace Queue\Engine;
use Queue\Engine;
use Model\ProductSingle\ProductSingleModel;
use Model\Activity\ProductModel;

ini_set('memory_limit','256M');
class ShopImageCut extends Engine
{
    public $imgMaxNum = 100;
    function execute($params)
    {
    	
        try {
        	//单品Modle
        	$productSingle = new ProductSingleModel();
        	$product= new ProductModel();
        	
        	//图片上传操作对象
        	$file = \Core\Factory::file();
        	$qiniu = \Core\Factory::file('qiniu');
        	$_SERVER['SERVER_ADDR'] = 'http://www.jinritemai.com';
        	
        	
        	$content_html = \Core\Common::getStorageKey(microtime(true), 'html');
        	
        	if($params['product_flag'] == 'single_shop'){
        		$data = $productSingle->getProductInfo($params['product_id']);
        	}else if($params['product_flag'] == 'activity_shop'){
        		$data = $product->getProdutOneInfo($params['product_id']);
        		echo "<pre>";
        		print_r($data);
        	}
        	
        	if(empty($data['detail_html'])){
        		$data['detail_html'] = 'No Pic !';
        		echo $data['detail_html']."\r\n";
        		return true;
        	}
        	
        	$html_status = $file->write($content_html, $data['detail_html']);
        	$params['create_name'] = $content_html;
        	
        	print_r($params);
        	
        	$abs_path_html = \Core\Common::getLocalAttachmentPathName($params['create_name']);
        	$src_img_path  = \Core\Common::getStorageKey($params['create_name'],'jpg');
        	$abs_path_jpg  = \Core\Common::getLocalAttachmentPathName($src_img_path);
        	
        	
        	$path_retry_html  = \Core\Common::getStorageKey($params['create_name'],'html');		//检测html文件

        	
        	//创建生成的文件路径
	        wkhtmltox_convert(
	        	'image',array(
		        	'screenWidth' => 810,
		        	//'quality' => 95,
		        	'out' => $abs_path_jpg,
		        	'in'  => $abs_path_html,
	        	)
        	);
	        //将合成图放入云存储
	        $qiniu->write($src_img_path, \Core\Common::getLocalAttachmentPathName($src_img_path));     //没问题!!!!!
	         
	        $pattern = '#<img[^>]+/?>#Uis';
	        $pre_content = file_get_contents($abs_path_html);
	        $imgNum = preg_match_all($pattern, $pre_content, $result);
	       
	        if($imgNum < $this->imgMaxNum){
                $this->imgMaxNum = $imgNum;		//图片自身数量
	        }
	        
	        //检测图片是否过大
	        while(filesize($abs_path_jpg) > 1024*1024*5) {
                $this->imgMaxNum -= 5;
	        
	        	//获取html文件内容
	        	$html_content = file_get_contents($abs_path_html);
	        		
	        	$pattern = '#<img[^>]+/?>#Uis';
	        	if (($imgNum = preg_match_all($pattern, $html_content, $result)) > $this->imgMaxNum){
	        		$lastImage  = $result[0][$this->imgMaxNum - 1];
	        		$position   = strpos($html_content, $lastImage) + strlen($lastImage);
	        		$subContent = substr($html_content, 0, $position);
	        
	        		$meta_str = '<meta http-equiv="Content-Type" content-type="text/html" charset="UTF-8" />';
	        		$style_str = "<style>body{width:790px;background: #fff;}</style>";
	        		$body_start = "<body><div style='margin:0 auto;width:100%;'>";
	        		$body_end = "</div></body>";
	        		
	        		
	        		//重复生成html
	        		$file->write($path_retry_html, $meta_str.$style_str.$body_start.$subContent.$body_end);
	        		
	        		//截取的html文件放入云存储
	        		###//$qiniu->write($path_retry_html, \Core\Common::getLocalAttachmentPathName($path_retry_html));
	        		
	        	}else{
	        		
	        		$subContent = $html_content;
	        
	        		//重复生成html
	        		$file->write($path_retry_html, $subContent);
	        		
	        		//截取的html文件放入云存储
	        		###//$qiniu->write($path_retry_html, \Core\Common::getLocalAttachmentPathName($path_retry_html));
	        	}
	        		
	        	//截取的图片
	        	$abs_path_retry_html = \Core\Common::getLocalAttachmentPathName($path_retry_html);
	        	
	        	$src_retry_img_path  = \Core\Common::getStorageKey($params['create_name'],'jpg');
	        	$abs_path_jpg = \Core\Common::getLocalAttachmentPathName($src_retry_img_path);
	        	
	        	
	        	wkhtmltox_convert(
		        	'image',array(
			        	'screenWidth' => 810,
			        	//'quality' => 95,
			        	'out' => $abs_path_jpg,
			        	'in'  => $abs_path_retry_html,
		        	)
	        	);
	        	
	        	//放入云存储
	        	$qiniu->write($src_retry_img_path, $abs_path_jpg);
	        	
	        	clearstatcache();	//清空文件缓存
	        }
	        
        	//②切割图片
        	$link= $abs_path_jpg;	//图片路径
        	$img = imagecreatefromjpeg($link);
        	
        	$maxW=imagesx($img);
        	$maxH = (imagesy($img)/$this->imgMaxNum);
        	
        	
        	list($width, $height, $type, $attr) = getimagesize($link);
        	$widthnum=ceil($width/$maxW);
        	$heightnum=ceil($height/$maxH);
        	
        	if($heightnum > $this->imgMaxNum){
        		$heightnum = $this->imgMaxNum;
        	}
        	
        	$iOut = imagecreatetruecolor ($maxW,$maxH);
        	//bool imagecopy ( resource dst_im, resource src_im, int dst_x, int dst_y, int src_x, int src_y, int src_w, int src_h )
        	//将 src_im 图像中坐标从 src_x，src_y 开始，宽度为 src_w，高度为 src_h 的一部分拷贝到 dst_im 图像中坐标为 dst_x 和 dst_y 的位置上。
        	
        	if(getimagesize($link) !== false){
                //入库前清空该商品下所有详情图片，type=2
                $productSingle->deleteDetailImg($params['product_id'],2);
        		for ($i=0;$i < $heightnum;$i++) {
        			for ($j=0;$j < $widthnum;$j++) {
        				imagecopy($iOut,$img,0,0,($j*$maxW),($i*$maxH),$maxW,$maxH);//复制图片的一部分
        				
        				$x_small_pic  = \Core\Common::getStorageKey($params['create_name'].$i,'jpg');      //生成小图名称
        				$x_small_pic_abspath = \Core\Common::getLocalAttachmentPathName($x_small_pic);
        				imagejpeg($iOut,$x_small_pic_abspath); //输出成0.jpg,1.jpg这样的格式
        
        				//放入云存储
        				$qiniu->write($x_small_pic, $x_small_pic_abspath);
//         				exit;
        				//插入
        				$data = array("product_id" => $params['product_id'],"shop_id" => $params['shop_id'],"pic" => $x_small_pic,"type" => 2,'sort' => intval($i+6));
        				$insert_id = $productSingle->insertProductImg($params['product_id'],$data);
        			}
        		}
        	}
        	
        	return true;
        	
        } catch (Exception $e) {
        	
            echo ($e->getMessage() . "\n" .
                   "Errno: ". $e->getCode() . "\n" .
                   "Params: " . var_export($params, true) . "\n" .
                   "In " . $e->getFile() . "\n" .
                   "On Line " . $e->getLine());
            return 0;
        }
    }
}