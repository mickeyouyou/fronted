<?php
namespace Queue;
/**
 * 队列异常
 *
 * @author: xudianyang
 * @version: QueueException.php, v-1.0.0, 2014-05-25 10:44 Created
 * @copyright Copyright (c) 2014 Beijing Jinritemai Technology Co.,Ltd.
 */

/**
 * Class QueueException
 *
 * 队列异常
 *
 * @package
 */
class Exception extends \Exception {}