<?php
namespace Queue;
/**
 * 队列日志操作
 *
 * @author: xudianyang
 * @version: QueueLog.php v-1.0.0, 2014-05-25 10:44 Created
 * @copyright Copyright (c) 2014 Beijing Jinritemai Technology Co.,Ltd.
 */

/**
 * Class QueueLog
 *
 * 队列日志类
 *
 * @package queue
 */
class Log
{
    protected static $_db;
    protected static $_table;

    protected static $_inited;

    static function init($db)
    {
        if (self::$_inited) return true;

        self::$_db     = $db;
        self::$_table  = 't_queue_log';
        self::$_inited = true;

        return true;
    }

    static function log($db, $queueid, $action, $arguments, $result, $message = '')
    {
        if (!self::$_inited) self::init($db);
        $result = (NULL === $result || !$result) ? '' : $result;
        $arguments = is_array($arguments) ? json_encode($arguments) : $arguments;

        return self::$_db->query(
            "INSERT INTO `" . self::$_table . "` (
             `queueid`, `action`, `arguments`, `result`, `message`
             ) VALUES (
             '{$queueid}', '{$action}', '{$arguments}', '{$result}', '{$message}'
            )");
    }
}