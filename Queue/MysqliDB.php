<?php
namespace Queue;
/**
 * 基于Mysqli简单的DB
 *
 * @author: xudianyang
 * @version: MysqliDB.php v-1.0.0, 2014-05-25 10:44 Created
 * @copyright Copyright (c) 2014 Beijing Jinritemai Technology Co.,Ltd.
 */

/**
 * Class MysqliDb
 *
 * @package queue
 */
class MysqliDB
{
    /**
     * @var object  instance of Mysqli Class
     */
    protected $mysqli;

    /**
     * @var object  instance of mysqliDb Class
     */
    private $instance = NULL;

    /**
     * @var  string query that was requested at last time when connecting to mysql
     */
    private $last_query = "";

    /**
     * 构造方法,初始化一个mysql连接
     *
     * @param $hostname string    hostname
     * @param $dbUser   string    database user name
     * @param $dbPassword  string database password
     * @param $dbname  string     optional database name
     * @param $port int optional port
     */
    public  function __construct($hostname, $dbUser, $dbPassword, $dbname = NULL, $port = 3306)
    {
        $this->connect($hostname, $dbUser, $dbPassword, $dbname, $port);
    }

    /**
     * 构造方法,初始化一个mysql连接
     *
     * @param $hostname string    hostname
     * @param $dbUser   string    database user name
     * @param $dbPassword  string database password
     * @param $dbname  string     optional database name
     * @param $port int optional port
     * @throws Exception
     *
     * @return void
     */
    public function connect($hostname, $dbUser, $dbPassword, $dbname = NULL, $port = 3306)
    {
        if (!isset($this->mysqli)) {
            if (is_null($dbname)) {
                $this->mysqli = new \Mysqli($hostname, $dbUser, $dbPassword, null, $port);
            } else {
                $this->mysqli = new \Mysqli($hostname, $dbUser, $dbPassword, $dbname, $port);
            }
            $this->mysqli->set_charset('utf8');
            $this->instance = $this;
            if ($this->mysqli->connect_errno) {
                throw new Exception($this->mysqli->connect_errno);
            }
        }
    }

    /**
     * returns query that was requested at last time when connecting to mysql
     *
     * @return string
     */
    public function getLastQuery()
    {
        return $this->last_query;
    }

    public function setLastQuery($query)
    {
        $this->last_query = $query;
    }

    /**
     * call method query of mysqli class stores query as last query
     *
     * @param $query string query
     *
     * @return mixed
     */
    public function query($query)
    {
        $this->setLastQuery($query);
        return $this->mysqli->query($query);
    }

    /**
     * returns instance of self
     *
     * @param $dbname  string optional database name
     *
     * @return object
     */
    public function getInstance($dbname = NULL)
    {
        return $this->instance;
    }

    /**
     * returns currently connected database
     *
     * @return string
     */
    function getCurrentDb()
    {
        return $this->getRowFromQuery("SELECT DATABASE() as db");
    }

    /**
     * returns insert id of last insert transaction
     *
     * @return int
     */
    public function getInsertId()
    {
        return $this->mysqli->insert_id;
    }

    /**
     * returns error  of last query transaction
     *
     * @return string
     */
    public function lastError()
    {
        return $this->mysqli->error;
    }

    /**
     * when no method is found in current class, it calls method of mysqli class if found in mysqli class
     *
     * @param $name  string            method name
     * @param $arguments  array        array of arguments
     * @throws BadMethodCallException  if no method is found in mysqli class
     *
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        if(method_exists($this->mysqli, $name)){
            return call_user_func_array(array($this->mysqli,$name), $arguments);
        }
        throw new BadMethodCallException("Called to undefined method $name!");
    }


    /**
     * when no property is found in current class, it returns property mysqli class
     *
     * @param  $name  string property name
     * @throws exception     if property is not found in mysqli Class
     *
     * @return mixed
     */
    public function __get($name)
    {
        if(property_exists($this->mysqli, $name)){
            return $this->mysqli->$name;
        }
        throw new Exception("Called to undefined property $name!");
    }

    /**
     * escapes string or elements of array
     *
     * @param $var   mixed          can be array or string   the string or array to be escaped
     * @param $recursionEscape bool if set to true and $var is array, escapes all elements of $var array
     *
     * @return mixed
     */
    public function escape($var, $recursionEscape = true)
    {
        if (!is_array($var)) {
            $res = $this->mysqli->real_escape_string($var);
        } else {
            $res = array();
            foreach ($var as $key => $value) {
                if ($recursionEscape) {
                    $res[$key] = $this->escape($value, $recursionEscape);
                } else {
                    $res[$key] = $value;
                }

            }
        }
        return $res;
    }


    /**
     * inserts new set of data to table by escaping data
     *
     * @param $table string  table name
     * @param array $data    data to be inserted in table
     *
     * @return bool
     */
    public function insert($table, $data)
    {
        $query_col = array();
        $query_v = array();
        $data = $this->escape($data);
        foreach ($data as $k => $v)
        {
            $query_col[] = "`" . $k . "`";
            if (is_array($v) && isset($v["type"]) &&  $v["type"] == 'MYSQL_FUNCTION') {
                $query_v[] = $v["value"];
            } else {
                $query_v[] = "'$v'";
            }


        }
        $query = "INSERT INTO " . $table . "(" . implode(", ", $query_col) . ")VALUES(" . implode(", ", $query_v) . ")";

        return  $this->query($query);
    }

    private function getPreparingWhereConditionFromArray($where)
    {

        $where = $this->escape($where);
        foreach ($where as $k => $v)
        {
            $query_w[] = "`" . $k .  "`=?";
        }
        return implode(" AND ", $query_w);

    }

    private function getPreparingWhereCondition($where)
    {
        if (empty($where)) {
            return "1=1";
        } else {
            return " WHERE ".$this->getPreparingWhereConditionFromArray($where)." ";
        }
    }

    /**
     * updates table using array as arguments
     *
     * @param $table  string  table to be updated
     * @param $data array
     * @param $where array where condition
     *
     * @return bool
     */
    public function update($table, $data, $where)
    {
        $query_v = array();

        foreach($data as $k => $v)
        {
            if (is_array($v) and isset($v["type"]) and  $v["type"] =='MYSQL_FUNCTION') {
                $query_v[] = "`" . $k .  "`=" .  $this->escape($v['value']);
                unset($data[$k]);
            } else {
                $query_v[] = "`" . $k .  "`=? ";
            }


        }
        $where_condition = $this->getPreparingWhereCondition($where);

        $query = "UPDATE " . $table . " SET " . implode(", ", $query_v) . "$where_condition";
        return $this->safeQuery($query, array_merge(array_values($data), array_values($where)));
    }

    /**
     * updates table using query as argument
     *
     * @param $query string
     *
     * @return mixed
     */
    public function updateFromQuery($query)
    {
        return $this->query($query);
    }


    /**
     * select rows of table using array as argument, use safeQuery method(preparing)
     *
     * @param $table  string  table to be updated
     * @param $fields array
     * @param $where array where condition
     *
     * @return mixed
     */
    private function selectFromArray($table, $fields, $where = array())
    {
        $query="SELECT ".implode(",", $fields)." FROM $table ";
        if (empty($where)) {
            return $this->query($query);
        }

        $where_condition = $this->getPreparingWhereCondition($where);

        $query .= $where_condition;

        return $this->safeQuery($query, array_values($where));
    }

    /**
     * counts num of results using array as argument, use safeQuery method(preparing)
     *
     * @param $table string    table to be updated
     * @param $where  array  where condition
     *
     * @return mixed
     */
    public function countRows($table, $where = array())
    {
        return $this->value($table, "count(*)", $where);
    }

    /**
     * returns first result of select query
     *
     * @param $res string   output of "mysql_query function" or "query method of mysqli class"
     * @throws BadMethodCallException
     *
     * @return array
     */
    private function getRow($res)
    {

        if ($res && $res->num_rows > 1) {
            throw new BadMethodCallException("Query returns more than row!");
        } else {
            $rows = $this->getMultiRow($res);
            return reset($rows);
        }

    }

    /**
     * returns first result of select query taking query as arguments
     *
     * @param  $query string query to be issued
     *
     * @return array
     */
    public function getRowFromQuery($query)
    {
        return $this->getRow($this->query($query));
    }

    /**
     * returns first result of select query taking array as arguments
     *
     * @param $table string    table name
     * @param $fields array array of fields
     * @param $where  array where condition
     *
     * @return array
     */
    public function row($table, $fields, $where = array())
    {

        return $this->getRow($this->selectFromArray($table, $fields, $where));
    }

    public function value($table, $field, $where = array())
    {
        $row = $this->row($table, array($field), $where);
        $value = reset($row);
        return $value;
    }

    public function column($table, $field, $where = array())
    {
        $rows = $this->multiRows($table, array($field), $where);
        return $this->convertRowsToColumn($rows);
    }

    public function valueFromQuery($query)
    {
        $row=$this->getRowFromQuery($query);
        return reset($row);
    }

    public function columnFromQuery($query)
    {
        $rows = $this->getMultiRowFromQuery($query);
        return $this->convertRowsToColumn($rows);
    }

    private function convertRowsToColumn($rows)
    {
        $results=array();
        foreach($rows as $row){
            $results[]=reset($row);
        }
        return $results;
    }

    public function getMultiRow($res)
    {
        if(!$res){
            return array();
        }else{
            $results=array();
            while($row = $res->fetch_assoc()){
                $results[]=$row;
            }
            return $results;
        }
    }

    public function getMultiRowFromQuery($query)
    {
        return $this->getMultiRow($this->mysqli->query($query));
    }

    public function multiRows($table,array $fields, $where=array())
    {
        return $this->getMultiRow($this->selectFromArray($table, $fields, $where));
    }

    public function delete($table, $where = array())
    {
        $where_condition = $this->getPreparingWhereCondition($where);
        $query = "DELETE FROM $table " . $where_condition;
        return $this->safeQuery($query, array_values($where));
    }

    public function __destruct()
    {
        if(isset($this->mysqli)){
            $this->mysqli->close();
        }
    }

    /**
     * prepares query, bind params and executes
     * @param string $query  query to be issued
     * @param (array or string) bindParams bind params
     * @param (array or string) (optional) paramType  types of bind params
     * @returns result of query
     */
    public function safeQuery($query, $bindParams, $paramType = NULL)
    {
        $this->setLastQuery($query);
        if (!is_array($bindParams)) {
            $bindParams = array($bindParams);
        }
        $stmt = $this->mysqli->prepare($query);

        if ($this->isArrayAssoc($bindParams)) {
            foreach ($bindParams as $key => $value) {
                $stmt->bind_param($key, $value);
            }
        } else {
            if (!is_null($paramType)) {
                if (is_array($paramType)) {
                    $params[0] = implode("", $paramType);
                } else {
                    $params[0] = $paramType;
                }

            } else {
                $params[0] = "";
            }

            foreach ($bindParams as $prop => $val) {
                if (is_null($paramType)) {
                    $params[0] .= $this->determineType($val);
                }

                array_push($params, $bindParams[$prop]);
            }

            call_user_func_array(array($stmt, 'bind_param'), $this->refValues($params));
        }


        if (!$stmt->execute()) {
            return FALSE;
        }
        if (stripos($query, "SELECT") !== FALSE) {
            $return_value = $stmt->get_result();
        } else {
            $return_value = TRUE;
        }
        $stmt->free_result();
        $stmt->close();
        return $return_value;

    }

    private function prepareSelect($numRows, $query, $bindParams, $paramType)
    {

        $query_result = $this->safeQuery($query, $bindParams, $paramType);
        if ($numRows == 1) {
            $results = $this->getRow($query_result);
        }else{
            $results = $this->getMultiRow($query_result);
        }
        return $results;
    }

    public function prepareMultiRow($query, $bindParams, $paramType=NULL)
    {
        return $this->prepareSelect(2, $query, $bindParams, $paramType);
    }

    /**
     * calls safeQuery and returns one row
     * @param string $query  query to be issued
     * @param (array or string) bindParams bind params
     * @param (array or string) (optional) paramType  types of bind params
     * @returns one row
     */
    public function prepareRow($query, $bindParams, $paramType=NULL)
    {
        return $this->prepareSelect(1, $query, $bindParams, $paramType);
    }

    /**
     * calls safeQuery and returns one value
     * @param string $query  query to be issued
     * @param (array or string) bindParams bind params
     * @param (array or string) (optional) paramType  types of bind params
     * @returns mixed
     */
    public function prepareValue($query, $bindParams, $paramType=NULL)
    {
        $row = $this->prepareRow($query, $bindParams, $paramType);
        return reset($row);
    }

    /**
     * @function prepareSelect calls safeQuery and returns an array of column
     * @param string $query  query to be issued
     * @param (array or string) bindParams bind params
     * @param (array or string) (optional) paramType  types of bind params
     * @returns array of values
     */
    public function prepareColumn($query, $bindParams, $paramType=NULL)
    {
        return $this->convertRowsToColumn($this->prepareRow($query, $bindParams, $paramType=NULL));
    }

    protected function determineType($item)
    {
        switch (gettype($item)) {
            case 'NULL':
            case 'string':
                return 's';
                break;

            case 'integer':
                return 'i';
                break;

            case 'blob':
                return 'b';
                break;

            case 'double':
                return 'd';
                break;
        }
        return '';
    }

    protected function refValues($arr)
    {
        if (strnatcmp(phpversion(), '5.3') >= 0) {
            $refs = array();
            foreach ($arr as $key => $value) {
                $refs[$key] = & $arr[$key];
            }
            return $refs;
        }
        return $arr;
    }

    private function isArrayAssoc($array){
        return (bool) count(array_filter(array_keys($array), 'is_string'));
    }
}
