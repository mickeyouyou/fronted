<?php
namespace Queue;
use Queue\Engine;

/**
 * 基于MySQL数据库的简单队列机制
 *
 * @author: xudianyang
 * @version: Queue.php v-1.0.0, 2014-05-25 10:44 Created
 * @copyright Copyright (c) 2014 Beijing Jinritemai Technology Co.,Ltd.
 */

/**
 * Class Queue
 *
 * 队列类，完成入队列、出队列等基本操作
 *
 * @package queue
 */
class Queue
{
    private $_db;
    private $_table;
    private $_error;
    private $_engine;
    private $_engineName;
    private $_maxTimes;

    public function __construct($engine, $db)
    {
        $this->_db    = $db;
        $this->_table = 't_queue';
        
        $this->_engineName = $engine;
        $class_name = $this->_engineName;

        $this->_engine = new $class_name;
        if (!$this->_engine instanceof Engine) {
            throw new Exception('incorrect queue engine type');
        }
    }

    static function getInstance($engine, $db)
    {
        static $instance;
		if (!isset($instance[$engine])) {
			$instance[$engine] = new self($engine, $db);
		}
		return $instance[$engine];
    }

    public function add($data, $delete = true)
    {
        $params = array(
            // engine
            $this->_engineName,
            // arguments
            json_encode($data),
            // nextrun
            time(),
            // created
            time(),
            // status
            1,
            // delete
            $delete ? 1 : 0,
            //product_id
            $data['product_id']
        );
        $result = $this->_db->safeQuery(
            "INSERT INTO `{$this->_table}` (
             `engine`, `arguments`, `nextrun`, `created`, `status`, `delete`,`product_id`
             ) VALUES (
             ?, ?, ?, ?, ?, ?,?
             )", $params);

        $queueid = $this->_db->getInsertId();

        Log::log($this->_db, $queueid, __FUNCTION__, $params, (int)$result);

        return $result;
    }

    public function start($queueid)
    {
        $params = array(
            // started
            time(),
            // ended
            0,
            // status
            2,
            // queueid
            $queueid
        );
        $result = $this->_db->safeQuery(
            "UPDATE `{$this->_table}`
             SET `started` = ?, `ended` = ?, `times` = `times` + 1, `status` = ?
             WHERE `queueid` = ? AND `status` <> 2", $params);

        Log::log($this->_db, $queueid, __FUNCTION__, $params, (int)$result);

        return $result;
    }

    public function end($queueid, $result, $status = 3)
    {
        $params = array(
            // ended
            time(),
            // result
            json_encode($result),
            // status
            (int)$status,
        );
        $addon = '';

        // need run again
        if (!$result && $status != 3) {
            $params[] = time() + mt_rand(1, 3) * 60;
            $addon = ', `nextrun` = ?';
        }

        // queueid
        $params[] = $queueid;

        $result = $this->_db->safeQuery(
            "UPDATE `{$this->_table}`
             SET `ended` = ?, `result` = ?, `status` = ? $addon
             WHERE `queueid` = ? AND `status` <> 3", $params);
        Log::log($this->_db, $queueid, __FUNCTION__, $params, (int)$result);

        return $result;
    }

    public function reset($queueid)
    {
        $params = array(
            // nextrun
            time(),
            // started
            0,
            // ended
            0,
            // status
            1,
            //queueid
            $queueid
        );
        $result = $this->_db->safeQuery(
            "UPDATE `{$this->_table}`
             SET `nextrun` = ?, `started` = ?, `ended` = ?, `status` = ?
             WHERE `queueid` = ?", $params);
        Log::log($this->_db, $queueid, __FUNCTION__, $params, (int)$result);

        return $result;
    }

    public function delete($queueid)
    {
        $params = array(
            // queueid
            $queueid
        );
        $result = $this->_db->safeQuery(
            "DELETE FROM `{$this->_table}`
             WHERE `queueid` = ? AND `status` <> 0", $params);
        Log::log($this->_db, $queueid, __FUNCTION__, $params, (int)$result);

        return $result;
    }

    public function execute($queueid)
    {
        $queue = $this->_db->getRowFromQuery("SELECT * FROM `{$this->_table}` WHERE `queueid` = {$queueid}");

        // queue not exists
        if (!$queue) {
            $this->_error = 'queue not exists';
            return false;
        }

        // queue not done
        if ($queue['status'] === 2 || $queue['started'] > $queue['ended']) {
            $this->_error = 'queue not done';
            return false;
        }

        // queue reached max try times limit
        if ($this->_maxTimes && $queue['times'] >= $this->_maxTimes)
        {
            $this->_error = 'queue reached max try times limit';
            return false;
        }

        // execute it
        $this->start($queueid);
        $arguments = (array) json_decode($queue['arguments'], true);
        $result = $this->_engine->execute($arguments);
        if (!$result) $this->_error = $this->_engine->error();
        Log::log($this->_db, $queueid, __FUNCTION__, $arguments, $result, $this->_error);

        $this->end($queueid, $result, ($result || $queue['times'] + 1 >= $this->_maxTimes) ? 3 : 1);

        // *require delete* after success executed or reached max try times limit
        if ($queue['delete'] && ($result || ($this->_maxTimes && ($queue['times'] + 1) >= $this->_maxTimes)))
        {
            $this->delete($queueid);
        }

        return $result;
    }

    public function interval($size, $maxTimes = 5)
    {
        $this->_maxTimes = $maxTimes;
        $result = "start: " . date('Y-m-d H:i:s', time()) . PHP_EOL;

        if ($size) {
            $queues = $this->_db->getMultiRowFromQuery(
                "SELECT `queueid` FROM `{$this->_table}`
                 WHERE `engine` = '". addslashes($this->_engineName) ."' AND `status` = 1 AND `nextrun` <= " . time() .
                " ORDER BY `nextrun` ASC LIMIT 0, {$size}"
            );

            $results = array('total' => 0, 'success' => 0, 'error' => 0);
            if ($queues) {
                $results['total'] = count($queues);
                foreach ($queues as $queue) {
                    $results[$this->execute($queue['queueid']) ? 'success' : 'error']++;
                }

            }
            $result .= "total: {$results['total']}, success: {$results['success']}, error: {$results['error']}" . PHP_EOL;
        } else {
            $result .= "size incorrect" . PHP_EOL;
        }

        $result .= "  end: " . date('Y-m-d H:i:s') . PHP_EOL . PHP_EOL;

        return $result;
    }

    public function error()
    {
        return $this->_error;
    }
}