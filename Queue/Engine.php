<?php
namespace Queue;
/**
 * 队列引擎（可以扩展各种需要后台执行的任务）
 *
 * @author: xudianyang
 * @version: QueueEngine.php v-1.0.0, 2014-05-25 10:44 Created
 * @copyright Copyright (c) 2014 Beijing Jinritemai Technology Co.,Ltd.
 */

/**
 * Class QueueEngine
 *
 * 队列工作任务基类
 *
 * @package
 */
abstract class Engine
{
    protected $_error;

    abstract function execute($params);

    function error()
    {
        return $this->_error;
    }
}