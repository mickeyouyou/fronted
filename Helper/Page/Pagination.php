<?php
/*
 *分页条
 */

namespace Helper\Page;

class Pagination extends \General\Db\Table\Pagination
{
    protected $url;

    public function __construct($currentPage = 1, $pageSize = 10)
    {
        parent::__construct($currentPage, $pageSize);
    }

    /*
     * @param string $url 基础URL
     * @param int $recordCount 总条目
     * @param int $currentPage	当前页码
     * @param int $pageSize 每页条数
     * @return Pagination
     */
    public static function create($url='', $currentPage=1, $pageSize = 20)
    {
        $p = new self($currentPage, $pageSize);
        $p->url = $url;
        $p->currentPage = $currentPage;
        $p->pageSize = $pageSize;

        return $p;
    }

    /**
     * 分页函数
     *
     * @param int $offset 页码显示数量控制（n*2+1）
     * @param bool $mode 是否转义
     * @param string $tag 页码标签
     * @return string
     */
    function getLinkString($offset = 2, $mode = false, $tag='li')
    {
        if($this->recordCount <= $this->pageSize) return '';
        $page = max(intval($this->currentPage), 1);
        $pages = ceil($this->recordCount/$this->pageSize);
        $page = min($pages, $page);
        $prepage = max($page-1, 1);
        $nextpage = min($page+1, $pages);
        $from = max($page - $offset, 2);
        if ($pages - $page - $offset < 1) $from = max($pages - $offset*2 - 1, 2);
        $to = min($page + $offset, $pages-1);
        if ($page - $offset < 2) $to = min($offset*2+2, $pages-1);
        $more = 1;
        if ($pages <= ($offset*2+5))
        {
            $from = 2;
            $to = $pages - 1;
            $more = 0;
        }
        $p_start_tag = '<'.$tag.'>';
        $p_end_tag = '</'.$tag.'>';
        $str = '';
        if ($page <= 1)
        {
            $str .= $p_start_tag.'<a href="javascript:void(0);" class="prev disable">上一页</a>'.$p_end_tag;
        }
        else
        {
            $str .= $p_start_tag.'<a href="'.self::pagesUrl($this->url, $prepage, $mode).'" class="prev">上一页</a>'.$p_end_tag;
        }
        $str .= $page == 1 ? $p_start_tag.'<a href="javascript:void(0);" class="current">1</a>'.$p_end_tag : $p_start_tag.'<a href="'.self::pagesUrl($this->url, 1, $mode).'">1'.($from > 2 && $more ? ' ...' : '').'</a>'.$p_end_tag;
        if ($to >= $from)
        {
            for($i = $from; $i <= $to; $i++)
            {
                $str .= $i == $page ? $p_start_tag.'<a href="javascript:void(0);" class="current">'.$i.'</a>'.$p_end_tag : $p_start_tag.'<a href="'.self::pagesUrl($this->url, $i, $mode).'">'.$i.'</a>'.$p_end_tag;
            }
        }
        $str .= $page == $pages ? $p_start_tag.'<a href="javascript:void(0);" class="current">'.$pages.'</a>'.$p_end_tag : $p_start_tag.'<a href="'.self::pagesUrl($this->url, $pages, $mode).'">'.($to < $pages-1 && $more ? '... ' : '').$pages.'</a>'.$p_end_tag;
        if ($page >= $pages)
        {
            $str .= $p_start_tag.'<a href="javascript:void(0);" class="next disable">下一页</a>'.$p_end_tag;
        }
        else
        {
            $str .= $p_start_tag.'<a href="'.self::pagesUrl($this->url, $nextpage, $mode).'" class="next">下一页</a>'.$p_end_tag;
        }
        return $str;
    }

    /**
     * 生成分页URL
     *
     * @param string $url 基础URL
     * @param int $page 分页页码
     * @param boolean $mode 是否转义
     * @return string
     */
    private static function pagesUrl($url, $page, $mode = false)
    {
        if (strpos($url, '$page') === false) {
            return self::urlQuery($url, array('page'=>$page), $mode);
        }
        return preg_replace('#{?\$page}?[\b]?#', $page, $url);
    }

    /**
     * 生成带参数的URL
     *
     * @param string $url 基础url
     * @param array $query 参数
     * @param bool $mode 是否转义
     * @return string
     */
    private static function urlQuery($url, $query = array(), $mode = false)
    {
        if ($query)
        {
            $data = parse_url($url);
            if (!$data) return false;
            if (isset($data['query']))
            {
                parse_str($data['query'], $q);
                $query = array_merge($q, $query);
            }
            $data['query'] = http_build_query($query);
            $url = self::httpBuildUrl($data, $mode);
        }
        return $url;
    }

    /**
     * 根据数组创建URL
     *
     * @param array $data
     * @param bool $mode 是否转义
     * @return string
     */
    private static function httpBuildUrl($data, $mode = false)
    {
        if (!is_array($data)) return false;
        $url = isset($data['scheme']) ? $data['scheme'].'://' : '';
        if (isset($data['user'])) $url .= $data['user'];
        if (isset($data['pass'])) $url .= ':'.$data['pass'];
        if (isset($data['user'])) $url .= '@';
        if (isset($data['host'])) $url .= $data['host'];
        if (isset($data['port'])) $url .= ':'.$data['port'];
        if (isset($data['path'])) $url .= $data['path'];
        if (isset($data['query'])) $url .= '?'.($mode ? str_replace('&', '&amp;', $data['query']) : $data['query']);
        if (isset($data['fragment'])) $url .= '#'.$data['fragment'];
        return $url;
    }
}
