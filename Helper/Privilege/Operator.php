<?php
namespace Helper\Privilege;

use Helper\Task\WorkerAble;

/*
 *操作者
 */
class Operator implements WorkerAble
{
    private $group;
    private $id;
    private $name;
    private $displayName;

    /**
     * 创建操作者
     * @param  {Int}  $id
     * @param  {String}  $name 名称
     * @param  {String}  $displayName 显示名
     * @return {Operator}
     */
    public static function create($group, $id, $name, $displayName='')
    {
        $o = new self();
        $o->id = $id;
        $o->name = $name;
        $o->displayName = $displayName;

        return $o;
    }

    /**
     * 把操作者信息转为独一无二的字符串 
     * 
     * @access public
     * @return void
     */
    public function getUniqueString()
    {
        return $this->id . '_' . $this->getDisplayName();
    }

    /**
     * 获取展示名
     * @return {String}
     */
    public function getDisplayName()
    {
        return empty($this->displayName)? $this->name : $this->displayName;
    }

    /**
     * 是否相同
     * @param  {Operator}  $o
     * @return {Boolean}
     */
    public function isEqual(WorkerAble $worker)
    {
        return $this->group == $worker->group && $this->id == $worker->id;
    }
}
