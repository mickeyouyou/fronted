<?php
/**
 * 日期
 *
 * @author Jinritemai
 * @copyright Beijing Jinritemai Technology Co.,Ltd.
 */
namespace Helper\Date;

/**
 * 日期类
 */
class Date
{
    /**
     * 时间转换为友好的字符串
     * @param  {Int}  $timestamp
     * @return {String}
     */
    static function timeToString ($timestamp)
    {
        $date = array();
        $now = array();
        $timeString = date('Y/n/j/G/i/s', $timestamp);
        list($date['year'], $date['month'], $date['day'], $date['hour'], $date['minute'], $date['second']) = explode('/', $timeString);
        list($now['year'], $now['month'], $now['day'], $now['hour'], $now['minute'], $now['second']) = explode('/', date('Y/n/j/G/i/s'));
        $date = array_map('intval', $date);
        $now = array_map('intval', $now);

        if ($date['year'] < $now['year']) {
            return date('Y-m-d', $timestamp);
        }
        if ($date['month'] < $now['month'] || $date['day'] + 2 < $now['day']) {
            return date('m-d', $timestamp);
        }
        if ($now['day'] - $date['day'] === 2) {
            return date('前天H:i', $timestamp);
        }
        if ($now['day'] - $date['day'] === 1) {
            return date('昨天H:i', $timestamp);
        }
        if ($date['hour'] < 12) {
            return date('上午h:i', $timestamp);
        }
        return date('下午h:i', $timestamp);
    }

    /**
     * 获取剩余时间
     * @param int $endTime
     * @param int $curTime
     * @return array
     */
    static function getLeftTime($endTime, $curTime){
        $strtime = array();
        $time = $endTime - $curTime;

        $strtime['day']= intval($time/86400) >= 0 ? intval($time/86400) : 0;
        $time = $time % 86400;

        $strtime['hour']= intval($time/3600) >= 0 ? intval($time/3600): 0;
        $time = $time % 3600;

        $strtime['min']= intval($time/60) >= 0 ? intval($time/60): 0;
        $time = $time % 60;
        return $strtime;
    }
}