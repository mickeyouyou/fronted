<?php
/**
 * 日期
 *
 * @author Jinritemai
 * @copyright Beijing Jinritemai Technology Co.,Ltd.
 */
namespace Helper\Date;

/**
 * 日期类(想和php自己的Date分开)
 */
class XDate
{
    const FORMAT_TIME_NORMAL = 'Y-m-d H:i:s';
    const FORMAT_DATE_NORMAL = 'Y-m-d';

    /**
     * 格式化时间戳
     * @param  {Int}  $timestamp
     * @param  {String}  $format
     * @return {String}
     */
    public static function formatTime($timestamp, $format = self::FORMAT_TIME_NORMAL)
    {
        return date($format, $timestamp);
    }

    /**
     * 获取当前时间 
     * 
     * @static
     * @access public
     * @return void
     */
    public static function now()
    {
        return self::formatTime(time());
    }

    /**
     * 获取当前日期 
     * 
     * @static
     * @access public
     * @return string
     */
    public static function today()
    {
        return date(self::FORMAT_DATE_NORMAL, time());
    }

    /**
     * 获取昨天日期 
     * 
     * @static
     * @access public
     * @return string
     */
    public static function yesterday()
    {
        return date(self::FORMAT_DATE_NORMAL, strtotime('1 days ago'));
    }

    /**
     * n天以后 
     * 
     * @param int $count 
     * @static
     * @access public
     * @return void
     */
    public static function daysAfter($n)
    {
        if(0 == $n)
        {
            return self::today();
        }
        else if(0 < $n)
        {
            return date(self::FORMAT_DATE_NORMAL, strtotime($n.' days'));
        }
        else
        {
            return days(self::FORMAT_DATE_NORMAL, strtotime("+{$n} days"));
        }

    }

    /**
     * 获取时间
     * 
     * @param mixed $timestamp 
     * @static
     * @access public
     * @return array('day', 'hour', 'minute', 'second')
     */
    public static function getRemainingTimes($timestamp)
    {
        $arr = array();
        $remainingTime = $timestamp - time();
        $arr['day'] = floor($remainingTime/86400);
        $remainingTime = $remainingTime%86400;
        $arr['hour'] = floor($remainingTime/3600);
        $remainingTime = $remainingTime%3600;
        $arr['minute'] = floor($remainingTime/60);
        $remainingTime = $remainingTime%60;
        $arr['second'] = floor($remainingTime/60);
        $remainingTime = $remainingTime%60;

        return $arr;
    }

    /**
     * 获取两个时间戳之间的天数差
     * @param timestamp $fisrtTime
     * @param timestamp $secondTime
     * @author
     */
    public static function getDays($firstTime, $secondTime)
    {
        $firstTimeArray  = getdate($firstTime);
        $secondTimeArray = getdate($secondTime);
        $firstDate  = mktime(12, 0, 0, $firstTimeArray['mon'], $firstTimeArray['mday'], $firstTimeArray['year']);
        $secondDate = mktime(12, 0, 0, $secondTimeArray['mon'], $secondTimeArray['mday'], $secondTimeArray['year']);
        return round(abs($firstDate - $secondDate)/86400);
    }

    /**
     * 时间字符串变为日期字符串 
     * 
     * @param string $time 
     * @static
     * @access public
     * @return string
     */
    public static function getDayByTime($time)
    {
        return self::formatTime(strtotime($time), self::FORMAT_DATE_NORMAL);
    }

}
