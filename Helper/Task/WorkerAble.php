<?php
namespace Helper\Task;

interface WorkerAble
{
    public function getDisplayName();
    public function isEqual(WorkerAble $worker);
}
