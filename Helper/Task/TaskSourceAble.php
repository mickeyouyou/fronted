<?php
namespace Helper\Task;


/**
 * 可以作为任务源的 
 */
interface TaskSourceAble
{
    /**
     * 获取任务集id
     * 
     * @static
     * @access public
     * @return string
     */
    public static function getTaskSetId4TaskSource();

    /**
     * 获取任务id 
     * 
     * @access public
     * @return string
     */
    public function getTaskId4TaskSource();
}
