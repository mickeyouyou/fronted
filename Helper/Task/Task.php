<?php
namespace Helper\Task;

use Core\Functions;
use Core\Factory;
use Helper\Date\XDate;
use Helper\Task\WorkerAble;
use Helper\Task\TaskSourceAble;
use Helper\Task\TaskSet;

/**
 * 任务
 */
class Task
{
    public $source;
    public $id;
    private $setId;
    private $createTime;
    private $worker;

    /**
     * 创建一个新任务
     *
     * @static
     * @access public
     * @return self
     */
    public static function create(TaskSourceAble $source)
    {
        $taskSetId = $source::getTaskSetId4TaskSource();
        $taskId = $source->getTaskId4TaskSource();
        $taskSet = TaskSet::getInstance($taskSetId);
        $task = $taskSet->getTask($taskId);
        if(!$task)
        {
            $task = new self();
            $task->id = $taskId;
            $task->setId = $taskSetId;
            $task->source = $source;
            $task->createTime = XDate::now();
            $task->worker = false;
            $task->_save();
        }

        return $task;
    }

    /**
     * 被worker删除任务
     *
     * @param mixed $taskId
     * @access public
     * @return void
     */
    public function removeBy(WorkerAble $worker)
    {
        Functions::assertTure($this->canRemoveBy($worker), '任务不可被删除');

        $redis = Factory::redis();
        $taskSet = TaskSet::getInstance($this->setId);
        $redis->HDEL($taskSet->getRedisHashKey(), $this->id);

        unset($this);
    }

    /**
     * 绑定worker
     *
     * @param WorkerAble $worker
     * @access public
     * @return void
     */
    public function bindWorker(WorkerAble $worker)
    {
        Functions::assertTure($this->canWork($worker), '无法绑定任务');
        if(!$this->isBindBy($worker))
        {
            $this->worker = $worker;
            $this->_save();
        }
    }

    /**
     * 释放worker
     *
     * @access public
     * @return void
     */
    public function releaseWorker(WorkerAble $worker)
    {
        if($this->isBinded())
        {
            Functions::assertTure($this->isBindBy($worker), '无权操作');
            $this->worker = false;
            $this->_save();
        }
    }

    /**
     * 获取当前工作者的描述
     * 
     * @access public
     * @return string
     */
    public function getWorkerDesc()
    {
        return $this->isBinded() ? $this->worker->getDisplayName() : '';
    }

    /**
     * 是否绑定
     *
     * @access public
     * @return bool
     */
    public function isBinded()
    {
        return false != $this->worker;
    }

    /**
     * 任务是否被指定worker绑定
     * 
     * @param mixed $worker 
     * @access public
     * @return bool
     */
    public function isBindBy($worker)
    {
        return $this->isBinded() && $this->worker->isEqual($worker);
    }

    /**
     * 任务是否可以被指定worker做
     *
     * @param WorkerAble $worker
     * @access public
     * @return bool
     */
    public function canWork(WorkerAble $worker)
    {
        return !$this->isBinded() || $this->isBindBy($worker);
    }

    /**
     * 任务是否可以被指定worker删除
     * 
     * @access public
     * @return bool
     */
    public function canRemoveBy(WorkerAble $worker)
    {
        return $this->canWork($worker);
    }

    /**
     * 存储任务
     *
     * @param mixed $taskId
     * @param mixed $task
     * @access private
     * @return void
     */
    private function _save()
    {
        $redis = Factory::redis();
        $taskSet = TaskSet::getInstance($this->setId);
        $redis->HSET($taskSet->getRedisHashKey(), $this->id, serialize($this));
    }

}
