<?php
namespace Helper\Task;

use General\Db\Table\Pagination;
use Core\Factory;
use Core\Functions;
use Helper\Task\Task;
use Helper\Task\WorkerAble;

/**
 * 任务集
 * 一个实例代表一种类型的任务集合
 */
class TaskSet
{
    private $id;

    /**
     * 获取任务集实例
     * 单例
     *
     * @static
     * @access public
     * @return self
     */
    public static function getInstance($taskSetId)
    {
        static $inses;
        if(false == isset($inses[$taskSetId]))
        {
            $t = new static();
            $t->id = $taskSetId;
            $inses[$taskSetId] = $t;
        }

        return $inses[$taskSetId];
    }

    /**
     * 分配一个任务给worker 
     * 
     * @param WorkerAble $worker 
     * @static
     * @access public
     * @return Task
     */
    public function assignTask(WorkerAble $worker)
    {
        $assignAbleTask = false;
        $this->_foreachTasks(
            function($task, $worker, &$assignAbleTask){
                $isContinue = true;
                if($task->canWork($worker))
                {
                    $assignAbleTask = $task;
                    $isContinue = false;
                }

                return $isContinue;
            }, $worker, $assignAbleTask
        );

        Functions::assertTure($assignAbleTask, '没有任务可以分配');
        $assignAbleTask->bindWorker($worker);

        return $assignAbleTask;
    }

    /**
     * 获取redis哈希key
     * 
     * @access public
     * @return void
     */
    public function getRedisHashKey()
    {
        return 'task:' . $this->id;
    }

    /**
     * 获取任务
     *
     * @param mixed $taskId
     * @access public
     * @return Task
     */
    public function getTask($taskId)
    {
        $redis = Factory::redis();

        return unserialize($redis->HGET($this->getRedisHashKey(), $taskId));
    }

    /**
     * 获取部分任务s
     *
     * @param Pagination $pagination
     * @access public
     * @return array(taskId => Task)
     */
    public function getPartTasks(Pagination $pagination)
    {
        $tasks = array();
        $offset = ($pagination->getCurrentPage() - 1 ) * $pagination->getPageSize();
        $limit = $pagination->getPageSize();
        if($limit > 0)
        {
            $redis = Factory::redis();
            $keys = $redis->HKEYS($this->getRedisHashKey());
            $partKeys = array_slice($keys, $offset, $limit);
            foreach($partKeys as $key)
            {
                $tasks[$key] = $this->getTask($key);
            }
        }

        return $tasks;
    }

    /**
     * 获取任务总数
     *
     * @access public
     * @return int
     */
    public function getTotalTaskCount()
    {
        $redis = Factory::redis();

        return $redis->HLEN($this->getRedisHashKey());
    }

    /**
     * 获取未分配的任务总数
     *
     * @access public
     * @return int
     */
    public function getUnSignedTaskCount()
    {
        $unSignedTaskCount = 0;
        $this->_foreachTasks(function($task, $null, &$unSignedTaskCount){
            if(!$task->isBinded())
            {
                $unSignedTaskCount++;
            }

            return true;
        }, null, $unSignedTaskCount);

        return $unSignedTaskCount;
    }


    /**
     * 获取被指定worker绑定的任务s 
     * 
     * @param WorkerAble $worker 
     * @access public
     * @return array
     */
    public function getTasksBindBy(WorkerAble $worker)
    {
        $tasks = array();
        $this->_foreachTasks(function($task, $worker, &$tasks){
            if($task->isBindBy($worker))
            {
                $tasks[$task->id] = $task;
            }

            return true;
        }, $worker, $tasks);

        return $tasks;
    }

    /**
     * 遍历本队列任务
     * 
     * @param mixed $func 
     * @param mixed $result 
     * @access private
     * @return void
     */
    private function _foreachTasks($func, $params, &$result)
    {
        $currentPage = 1;
        $pageSize = 200;
        do {
            $pagination = new Pagination($currentPage++, $pageSize);
            $tasks = $this->getPartTasks($pagination);
            foreach($tasks as $taskId => $task)
            {
                if(!$func($task, $params, $result))
                {
                    return;
                }
            }
        } while(!empty($tasks));
    }

}
