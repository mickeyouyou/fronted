<?php
namespace Helper\Lock;

use Core\Factory;

/*
 *页面锁
 *用于某一页面或一组页面具有排他性
 */
class PageLock
{
    private $_key;

    const CACHE_KEY_PRE = 'PageLock:Locks:';//键值的缓存前缀

    /**
     * 根据锁定者获取锁列表 
     * 
     * @param mixed $locker 
     * @static
     * @access public
     * @return array
     */
    public static function getLocksByLocker($locker)
    {
        $redis = Factory::redis();
        $locks = array();
        $lockerIndexKey = self::_getLockerIndexKey($locker);
        foreach($redis->HKEYS($lockerIndexKey) as $key)
        {
            if($redis->has($key))
            {
                $uniqueKey = self::_getUniqueKey($key);
                $locks[] = self::getInstance($uniqueKey);
            }
            else
            {
                $redis->HDEL($lockerIndexKey, $key);
            }
        }

        return $locks;
    }

    /**
     * 得到锁的实例
     * 
     * @param mixed $uniqueKey 
     * @static
     * @access public
     * @return self
     */
    public static function getInstance($uniqueKey)
    {
        $l = new self();
        $l->_key = $uniqueKey;

        return $l;
    }

    /**
     * 获取锁定者列表的索引key 
     * 
     * @param mixed $locker 
     * @static
     * @access private
     * @return string
     */
    private static function _getLockerIndexKey($locker)
    {
        return 'PageLock:Lockers:' . $locker;
    }

    /**
     * 根据缓存key反解到唯一key 
     * 
     * @param mixed $key 
     * @static
     * @access private
     * @return string
     */
    private static function _getUniqueKey($key)
    {
        return substr($key, strlen(self::CACHE_KEY_PRE));
    }

    /**
     * 获取唯一key值 
     * 
     * @access public
     * @return string
     */
    public function getUniqueKey()
    {
        return $this->_key;
    }

    /**
     * isLocked 
     * 是否锁住
     * 
     * @access public
     * @return bool
     */
    public function isLocked()
    {
        $redis = Factory::redis();

        return $redis->has($this->_getCacheKey());
    }

    /**
     * 是否被指定锁定者锁住
     * 
     * @param mixed $locker 
     * @access public
     * @return bool
     */
    public function isLockedBy($locker)
    {
        return $locker == $this->getLocker();
    }

    /**
     * 锁住页面
     * 页面没有被锁定则锁定页面，返回true
     * 页面如果已锁定则返回false
     * 假如同一锁定者再次申请同一个锁,则返回true,并刷新锁的时间和类型
     * 
     * @param mixed 超时时间(秒)
     * @param mixed 锁定者 
     * @param mixed 锁定类型
     * @access public
     * @return bool
     */
    public function lockPage($expireSecond, $locker, $lockType)
    {
    	
        $redis = Factory::redis();
        $cacheKey = $this->_getCacheKey();

        $result = true;
        if($redis->has($cacheKey))
        {
            list($beforeLocker, $beforeLockType) = $this->_splitCacheContent();

            
            if($beforeLocker == $locker)
            {
            	self::_eidtLock($locker, $lockType, $cacheKey, $expireSecond);
            	$redis->EXPIRE($cacheKey, $expireSecond);
            }
            else
            {
                $result = false;
            }
        }
        else
        {
        	self::_eidtLock($locker, $lockType, $cacheKey, $expireSecond);       	
        }
        return $result;
    }
    
    /**
     * 重新设置锁的时间和锁定类型
     * @return string
     */
    private function _eidtLock($locker, $lockType, $cacheKey, $expireSecond) 
    {
    	$lockerIndexKey = self::_getLockerIndexKey($locker);
    	$redis = Factory::redis();
    	$value = $this->_assemblyCacheContent($locker, $lockType);
    	
    	$result = $redis->set($cacheKey, $value, $expireSecond);
    	$redis->HSET($lockerIndexKey, $cacheKey, $value);
    	$redis->EXPIRE($lockerIndexKey, $expireSecond);
    }
    
    /**
     * 释放页面
     * 
     * @access public
     * @return void
     */
    public function releasePage()
    {
        $redis = Factory::redis();

        $redis->remove($this->_getCacheKey());
    }

    /**
     * 获取锁定者
     * 
     * @access public
     * @return string
     */
    public function getLocker()
    {
        list($locker, $lockType) = $this->_splitCacheContent();

        return $locker;
    }

    /**
     * 获取锁定类型
     * 
     * @access public
     * @return string
     */
    public function getLockType()
    {
        list($locker, $lockType) = $this->_splitCacheContent();

        return $lockType;
    }

    /**
     * 获取缓存键
     * 
     * @access private
     * @return string
     */
    private function _getCacheKey()
    {
        return self::CACHE_KEY_PRE . $this->_key;
    }

    /**
     * 把锁定信息组装成缓存值
     * 
     * @param mixed $locker 
     * @param mixed $lockType 
     * @access private
     * @return string
     */
    private function _assemblyCacheContent($locker, $lockType)
    {
        return json_encode(array(
            $locker,
            $lockType,
        ));
    }

    /**
     * 把缓存值拆分成锁定信息
     * 
     * @access private
     * @return array
     */
    private function _splitCacheContent()
    {
        $redis = Factory::redis();
        $lockInfo = array(
            '',
            '',
        );
        $content = $redis->get($this->_getCacheKey());
        $arr = json_decode($content);
        if(2 == count($arr))
        {
            $lockInfo = $arr;
        }

        return $lockInfo;
    }

}
