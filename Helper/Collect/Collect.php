<?php

/**
 * 天猫淘宝商品抓取
 *
 * @author kongdeshi
 * @copyright Copyright (c) Beijing Jinritemai Technology Co.,Ltd.
 * @version Id:2.0.0, TransferPool.php, 2014-9-1 下午6:35:42 created
 */

namespace Helper\Collect;

use \Helper\Collect\Trans;
use \Helper\Collect\Snoopy;

define('LIVE_PROXY', '10.160.29.21:65002');
define('LIVE_PROXY_DOMAIN', 'www.jinritemai.com');

/**
 * 商品信息抓取类
 */
class Collect {

    private $detailImgNum;//详情页抓取图片个数
    private $trans;

    public function __construct() {
        $this->detailImgNum = 100;
        $this->trans = new Trans();
    }

    /**
     * JAE抓取和页面抓取合并
     * @param unknown $num_iid
     * @return unknown
     */
    public function getProductDetail($num_iid) {
        $normal_get = $this->getProduct($num_iid);      //产品信息
        $api_get = $this->getItemDetail($num_iid);   //产品详细信息

        $item['title'] = empty($api_get['title']) ? $normal_get['title'] : $api_get['title'];
        $item['shop_price'] = empty($api_get['shop_price']) ? $normal_get['shop_price'] : $api_get['shop_price'];
        $item['pic_url'] = empty($api_get['pic_url']) ? $normal_get['pic_url'] : $api_get['pic_url'];
        $item['detail_url'] = empty($api_get['detail_url']) ? $normal_get['detail_url'] : $api_get['detail_url'];
        $item['click_url'] = empty($api_get['click_url']) ? $normal_get['click_url'] : $api_get['click_url'];
        $item['nick'] = empty($api_get['nick']) ? $normal_get['nick'] : $api_get['nick'];
        $item['dc_price'] = empty($api_get['dc_price']) ? $normal_get['dc_price'] : $api_get['dc_price'];
        $item['saled_num'] = !empty($api_get['saled_num']) ? $api_get['saled_num'] : !empty($normal_get['saled_num']) ? $normal_get['saled_num'] : "";
        $item['parameter'] = !empty($normal_get['parameter']) ? $normal_get['parameter'] : '';

        $item['nick'] = $api_get['nick'] ? $api_get['nick'] : $normal_get['nick'];

        foreach ($item as $key => $val) {
            $item[$key] = str_replace(" ", "", $val);
        }
        $item['content'] = $api_get['content'] ? $api_get['content'] : $normal_get['content'];

        //商品sku
        $item['sku'] = $this->getSku($num_iid);

        //生成商品详情
        $meta_str = '<meta http-equiv="Content-Type" content-type="text/html" charset="UTF-8" />';
        $style_str = "<style>body{width:790px;background: #fff;}</style>";
        $body_start = "<body><div style="."margin:0 auto;width:100%;".">";
        $body_end = "</div></body>";

        //只截取50个
        $pattern = '#<img[^>]+/?>#Uis';
        if (($imgNum = preg_match_all($pattern, $item['content'], $result)) > $this->detailImgNum) {
            $lastImage = $result[0][$this->detailImgNum];
            $position = strpos($item['content'], $lastImage) + strlen($lastImage);
            $subContent = substr($item['content'], 0, $position);
        } else {
            $subContent = $item['content'];
        }

        $file = \Core\Factory::file();
        $content_html = \Core\Common::getStorageKey(microtime(true), 'html');

        //@author by gaoqiang 
        $item['content_html'] = $content_html;
        $item['detail_html']  = $meta_str . $style_str . $body_start . $subContent . $body_end;   //商品详情图片存放字段
        return $item;
    }

    /**
     * 抓取商品评论
     * @param int $num_iid
     * @param string $short_name
     * @return string
     */
    public function getProductComment($num_iid, $short_name) {
        $comment = array();
        for ($i = 1; $i < 5; $i++) {
            //调用评论
            $comment_bak = $this->getItemComment($num_iid, $short_name, $i);
            //空页面停止
            if (!$comment_bak) {
                break;
            }
            $comment = array_merge($comment_bak, $comment);

            if (count($comment_bak) < 16) {
                break;
            }
            if (count($comment) > 50) {
                break;
            }
        }

        $comment_data = count($comment) > 50 ? array_slice($comment, 0, 50) : $comment;

        $find = array("年", "月", "日");
        $replace = array("-", "-");
        $new_comment = '';
        if ($short_name == "taobao") {
            foreach ($comment_data as $key => $val) {
                $new_comment[$key]['user_id'] = $val->user->userId ? $val->user->userId : 0;
                $new_comment[$key]['sku'] = $val->auction->sku;
                $new_comment[$key]['create_time'] = str_replace($find, $replace, $val->date);
                $new_comment[$key]['user_name'] = $val->user->nick;
                $new_comment[$key]['content'] = $val->content;
                $new_comment[$key]['rateId'] = $val->rateId;
                $new_comment[$key]['num_iid'] = $num_iid;
            }
        } elseif ($short_name == "tmall") {
            foreach ($comment_data as $key => $val) {
                $new_comment[$key]['user_id'] = substr($val->id, -9, 9) ? substr($val->id, -9, 9) : 0;
                $new_comment[$key]['sku'] = $val->auctionSku;
                $new_comment[$key]['create_time'] = $val->rateDate;
                $new_comment[$key]['user_name'] = $val->displayUserNick;
                $new_comment[$key]['content'] = $val->rateContent;
                $new_comment[$key]['rateId'] = $val->id;
                $new_comment[$key]['num_iid'] = $num_iid;
            }
        }
        if (is_array($new_comment)) {
            foreach ($new_comment as $key => $val) {
                $new_comment[$key]['value'] = base64_encode(serialize($val));
            }
        } else {
            $new_comment = "";
        }

        return $new_comment;
    }

    /**
     * 单页面抓取评论
     * @param int $num_iid
     * @param string $type
     * @param number $page
     * @return boolean|unknown
     */
    public function getItemComment($num_iid, $type = 'taobao', $page = 1) {
        $url = 'http://item.taobao.com/item.htm?id=' . $num_iid;
        $content = $this::getUrlContent($url);
        if (!$content) {
            return FALSE;
        }
        $itemid = $this->preGetOne('itemId:\"(\d+)\"', $content);
        $userid = $this->preGetOne('userid=(\d+?)', $content);
        $sellerid=$this->preGetOne('sellerId:\"(\d+)\"',$content);
        
        if(!isset($itemid[1])){
        	$itemid[1] = 0;
        }
        if(!isset($userid[1])){
        	$userid[1] = 0;
        }
        if(!isset($sellerid[1])){
        	$sellerid[1] = 0;
        }
        $urlarr = array(
            'tmall' => 'http://rate.tmall.com/list_detail_rate.htm?currentPage=' . $page . '&itemId=' . $itemid[1].'&sellerId='.$sellerid[1]
            ,'taobao' => 'http://rate.taobao.com/feedRateList.htm?siteID=4&currentPageNum=' . $page . '&userNumId=' . $userid[1] . '&auctionNumId=' . $itemid[1]
        );
        $rateurl = $urlarr[$type];
        $getcontent = $this::getUrlContent($rateurl);
        
        $comment_json = iconv('GBK', 'UTF-8//IGNORE', preg_replace('/\n/is', '', $getcontent));
        
        if ($type == "tmall") {
            $lpos = strpos($comment_json, "{");
            $rpos = strrpos($comment_json, "}");
            $comment_json = substr($comment_json, $lpos);
            $comment_info = json_decode($comment_json);
            $comment_bak = $comment_info->rateList;
        } elseif ($type == "taobao") {
            $lpos = strpos($comment_json, "(");
            $rpos = strrpos($comment_json, ")");
            $comment_json = substr($comment_json, $lpos + 1, $rpos - $lpos - 1);
            $comment_info = json_decode($comment_json);
            $comment_bak = $comment_info->comments;
        }
        
        return $comment_bak;
    }

    /**
     * preg 封装
     * @param unknown $pattern
     * @param unknown $subject
     * @return unknown
     */
    private function preGetOne($pattern, $subject) {
        preg_match('/' . $pattern . '/isU', $subject, $return);
        return $return;
    }

    /**
     * 过滤 封装 html和\s
     * @param unknown $str
     * @return mixed
     */
    private function replaceDiv($str) {
        $preg = preg_replace('/<.+?>/', '', $str);
        return preg_replace('/\s+/', '', $preg);
    }

    /**
     * 抓取商品所在店铺信息
     * @param int $num_iid
     * @param string $type
     * @return boolean|StdClass
     */
    public function getShopDetail($num_iid, $type = 'tmall') {
        // header("Content-type: text/html; charset=gbk");
        $url = 'http://item.taobao.com/item.htm?id=' . $num_iid;
        $content = $this->getUrlContent($url);
        if (!$content) {
            return FALSE;
        }

        $pregshopid = $this->preGetOne('shopId:\"(\d+)\"', $content);
       
        if(isset($pregshopid[1])){
        	$shopid = $pregshopid[1];
        }else{
        	$shopid = 0;
        }

        $pregshopname = $this->preGetOne('tb-shop-name\">(.*)<\/div>', $content);

        if ($pregshopname) {
            //淘宝店铺信息抓取
            $shopname = $this->replaceDiv($pregshopname[1]);
            $pregshophref = $this->preGetOne('href=\"(.*)\"', $pregshopname[1]);
            $shophref = $pregshophref[1];
        } else {
            //天猫店铺信息抓取
            $pregshopname = $this->preGetOne('<a class=\"slogo-shopname\"(.*)<\/a>', $content);
            if ($pregshopname) {
                $shopname = $this->replaceDiv($pregshopname[0]);
                $pregshophref = $this->preGetOne('href=\"(.*)\"', $pregshopname[1]);
                $shophref = $pregshophref[1];
            } else {
                $shopname='';
                $shophref = '';
            }
        }


        //$score = $this->preGetOne('class=\"tb-shop-seller\">(.*)\"tb-shop-icon\">', $content);

        //新版本
        $score = $this->preGetOne('class=\"tb-shop-rate\">(.*)\"tb-shop-info-ft\">', $content);

        if ($score) {
            //淘宝店铺描述抓取
            $content_taobao = iconv('GBK', 'UTF-8//IGNORE', $score[1]);
            $pregshopdesc = $this->preGetOne('<dt>描述<\/dt>(.*)<\/a>', $content_taobao);
            $shopdesc = $this->replaceDiv($pregshopdesc[1]);

            $pregshopservice = $this->preGetOne('<dt>服务<\/dt>(.*)<\/a>', $content_taobao);
            $shopservice = $this->replaceDiv($pregshopservice[1]);

            $pregshoplogistic = $this->preGetOne('<dt>物流<\/dt>(.*)<\/a>', $content_taobao);
            $shoplogistic = $this->replaceDiv($pregshoplogistic[1]);
        } else {
            //天猫店铺抓取
            $content_tmall = iconv('GBK', 'UTF-8//IGNORE', $content);
            $pregshopdesc = $this->preGetOne('描 述<\/div>(.*)<\/span>', $content_tmall);
            if ($pregshopdesc) {
                $shopdesc = $this->replaceDiv($pregshopdesc[1]);
            } else {
                $shopdesc = '';
            }
            $pregshopservice = $this->preGetOne('服 务<\/div>(.*)<\/span>', $content_tmall);
            if ($pregshopservice) {
                $shopservice = $this->replaceDiv($pregshopservice[1]);
            } else {
                $shopservice = '';
            }
            $pregshoplogistic = $this->preGetOne('物 流<\/div>(.*)<\/span>', $content_tmall);
            if ($pregshoplogistic) {
                $shoplogistic = $this->replaceDiv($pregshoplogistic[1]);
            } else {
                $shoplogistic = '';
            }
        }

        $shophref = $shophref ? $shophref : "0";
        $shopdesc = $shopdesc ? $shopdesc : "0";
        $shopservice = $shopservice ? $shopservice : "0";
        $shoplogistic = $shoplogistic ? $shoplogistic : "0";
        return (object) array(
                    'shop' => (object) array(
                        'id' => $shopid
                        , 'name' => $shopname
                        , 'href' => $shophref
                    )
                    , 'score' => (object) array(
                        'desc' => $shopdesc
                        , 'service' => $shopservice
                        , 'logistics' => $shoplogistic
                    )
        );
    }

    /**
     * 天猫淘宝商品页面抓取
     * @param unknown $num_iid
     * @return boolean|unknown
     */
    public function getProduct($num_iid) {
        $url = 'http://item.taobao.com/item.htm?id=' . $num_iid;
        $content = $this::getUrlContent($url);

        if (!$content)
            return FALSE;
        $content = @iconv("gbk", "utf-8//IGNORE", $content);

        $c = $this->trans->t2c($content);

        @preg_match('/<title>(.*)<\/title>/i', $c, $a);
        if (!isset($a[1])) {
            return FALSE;
        }
        if (strpos($a[1], '-淘宝网') !== FALSE) {
            $is_taobao = TRUE;
        } else {
            $is_taobao = FALSE;
        }
        $title = str_replace('-tmall.com天猫', '', str_replace('-淘宝网', '', $a[1]));
        $shop_url = FALSE;
        $shop_price = $dc_price = $volume = 0;
        @preg_match('/<a(.*?)href="(.*?)"(.*?)>(.*?)进入店铺(.*?)<\/a>/i', $c, $a);
        if (isset($a[2]) && $a[2]) {
            $shop_url = $a[2];
        } else {
            @preg_match('/shopUrl:"(.*?)"/i', $c, $a);
            if (isset($a[1]) && $a[1]) {
                $shop_url = $a[1];
            }
        }
        if ($shop_url) {
            $shop_url = parse_url($shop_url);
            $shop_url = $shop_url['scheme'] . '://' . $shop_url['host'] . '/search.htm';
            $shop_url .= '?keyword=' . urlencode(iconv("utf-8", "gbk//IGNORE", $title));
            $c1 = $this::getUrlContent($shop_url);
            $c1 = @iconv("gbk", "utf-8//IGNORE", $c1);
            $c1 = $this->trans->t2c($c1);
            $c1 = @preg_replace(array("'([\r\n\t]+)'"), array(""), $c1);

            if ($rule = $this->getRule($c1)) {
                $item_html = '';
                foreach ($rule['items'] as $v) {
                    if (strpos($v, '=' . $num_iid) !== FALSE) {
                        $item_html = $v;
                        break;
                    }
                }
                //             exit;
                if ($item_html) {
                    if ($rule['item_rule']['s_price_rule'])
                        preg_match($rule['item_rule']['s_price_rule'], $item_html, $a);
                    else
                        $a = FALSE;
                    if (isset($a[1]) && $a[1]) {
                        $shop_price = trim($a[1]);
                        preg_match($rule['item_rule']['c_price_rule'], $item_html, $a);
                        if (isset($a[1]) && $a[1]) {
                            $dc_price = trim($a[1]);
                        }
                    } else {
                        preg_match($rule['item_rule']['c_price_rule'], $item_html, $a);
                        if (isset($a[1]) && $a[1]) {
                            $shop_price = trim($a[1]);
                        }
                    }
                    preg_match($rule['item_rule']['sale_rule'], $item_html, $a);
                    if (isset($a[1])) {
                        $volume = trim($a[1]);
                    }
                }
            }
        }
        if ($shop_price == 0) {
            preg_match('/"price" : "(.*?)"/i', $c, $a);
            if (isset($a[1])) {
                $shop_price = $a[1];
            }
        }
        if ($shop_price == 0) {
            preg_match('/price:(.*?),/i', $c, $a);
            if (isset($a[1])) {
                $shop_price = $a[1];
            }
        }
        if ($shop_price == 0) {
            preg_match('/"defaultItemPrice":"(.*?)"/i', $c, $a);
            if (isset($a[1])) {
                $shop_price = $a[1];
            }
        }
        if ($shop_price == 0)
            return FALSE;
        if ($is_taobao) {
            $r_a = $this->getTaobaoItem($c);

            //匹配商品详情下各参数+淘宝链接
            preg_match('/<ul class=\"attributes-list\">(.*?)<\/ul>/is', $content, $re);
            preg_match_all('/<li title=(.*?)>(.*?)<\/li>/is', $re[1], $lt_taobao);  // 匹配li中的内容
            if (!empty($lt_taobao[2])) {
                foreach ($lt_taobao[2] as &$v) {
                    $v = $this->unescapeCh($v);
                }
                $r_a['parameter'] = json_encode($lt_taobao[2]);
            }
        } else {
            $url = 'http://detail.tmall.com/item.htm?id=' . $num_iid;
            $r_a = $this->getTmallItem($c);

            //匹配商品详情下各参数+天猫链接
            preg_match('/<ul id=\"J_AttrUL\">(.*?)<\/ul>/is', $content, $re);
            preg_match_all('/<li title=(.*?)>(.*?)<\/li>/is', $re[1], $lt_tmall);  // 匹配li中的内容
            if (!empty($lt_tmall[2])) {
                foreach ($lt_tmall[2] as &$v) {
                    $v = $this->unescapeCh($v);
                }
                $r_a['parameter'] = json_encode($lt_tmall[2]);
            }
        }

        $r_a['title'] = $title;
        $r_a['detail_url'] = $url;
        $r_a['shop_price'] = $shop_price;
        $r_a['dc_price'] = $dc_price;
        $r_a['volume'] = $volume;
        $r_a['num_iid'] = $num_iid;
        return $r_a;
    }

    /**
     * jae接口抓取
     * @param unknown $num_iid
     * @return unknown
     */
    public function getItemDetail($num_iid) {

        $url = 'http://toutiao.ai.m.jaeapp.com/view/index.php?num_iid=' . $num_iid;
        $content = $this::getUrlContent($url);
        $new_content = mb_convert_encoding($content, 'UTF-8', 'GBK');
        $content = json_decode($new_content);
        $item = array();
        if ($content) {
            $item['title'] = isset($content->title) ? $content->title : '';
            $item['shop_price'] = isset($content->price) ? $content->price : '';
            $item['nick'] = isset($content->nick) ? $content->nick : '';
            $item['click_url'] = isset($content->click_url) ? $content->click_url : '';
            $item['pic'] = isset($content->item_imgs->item_img) ? $content->item_imgs->item_img : '';
            $item['detail_url'] = isset($content->click_url) ? $content->click_url : "";
            $item['dc_price'] = isset($content->dc_price) ? $content->dc_price : "";
            $item['saled_num'] = isset($content->saled_num) ? $content->saled_num : "";
            if (is_array($item['pic'])) {
                foreach ($item['pic'] as $val) {
                    $item['pic_url'][] = $val->url;
                }
            } else {
                $item['pic_url'] = '';
            }
            $item['saled_num '] = isset($content->saled_num) ? $content->saled_num : "";
            $item['content'] = isset($content->desc) ? $content->desc : "";
        }
        return $item;
    }

    /**
     * 获取商品sku
     * @param unknown $num_iid
     * @return unknown
     */
    public function getSku($num_iid) {
        $url = 'http://a.m.tmall.com/ajax/sku.do?item_id=' . $num_iid;
        $sku_content = $this::getContent($url);
        return $sku_content;
    }

    /**
     * url正则
     * @param unknown $c
     * @return multitype:unknown |boolean
     */
    private function getRule($c) {
        $fetch_rule_config = array(
            array(
                'item_rule' => '/<dl class=(.*?)>.*?<\/dl>/i',
                'c_price_rule' => '/<span class="c-price">(.*?)<\/span>/i',
                's_price_rule' => '/<span class="s-price">(.*?)<\/span>/i',
                'sale_rule' => '/<span class="sale-num">(.*?)<\/span>/i',
                'img_rule' => '/data\-ks\-lazyload="(.*?)"/i',
                'hplink_rule' => '/<a class="item-name"(.*?)href="(.*?)"(.*?)>(.*?)<\/a>/i',
            ),
            array(
                'item_rule' => '/<div class="item">.*?<\/li>/i',
                'c_price_rule' => '/<div class="price">.*?<strong>(\d+(\.\d+)?).*?<\/strong>.*?<\/div>/i',
                's_price_rule' => '',
                'sale_rule' => '/<div class="sales-amount">.*?<em>(\d+)<\/em>.*?<\/div>/i',
                'img_rule' => '/data\-ks\-lazyload="(.*?)"/i',
                'hplink_rule' => '/<div class="desc"><a(.*?)href="(.*?)" class=(.*?)>(.*?)<\/a>/i'
            )
        );
        foreach ($fetch_rule_config as $row) {
            preg_match_all($row['item_rule'], $c, $a);
            if (isset($a[0][0])) {
                return array('items' => $a[0], 'item_rule' => $row);
            }
        }
        return FALSE;
    }

    /**
     * 淘宝抓取规则
     * @param unknown $c
     * @return multitype:Ambigous <string, unknown> string multitype:string
     */
    private function getTaobaoItem($c) {
        $nick = '';
        preg_match('/sellerNick:"(.*?)"/i', $c, $a);
        if (isset($a[1])) {
            $nick = $a[1];
        }
        $pic_url = array();
        preg_match_all('/<img data-src="(.*?)" \/>/i', $c, $a);
        if (isset($a[1])) {
            foreach ($a[1] as $v) {
                $pic_url[] = substr($v, 0, strrpos($v, '_'));
            }
        }
        $content = '';
        $is_desc = '1';
        if ($is_desc == 1) {
            //preg_match('/"apiItemDesc":"(.*?)"/i', $c, $a);
            preg_match('/"https:" === location.protocol(.*?)"(.*?)"/i', $c, $a);
            if(isset($a[2]) && substr($a[2],0,2) == "//"){
                $url = "http://".substr($a[2],2);
                $a[1] = str_replace("desc.alicdn.com","dsc.taobaocdn.com",$url);
            }

            if (isset($a[1])) {
                $content = $this::getUrlContent($a[1]);
                $content = @iconv("gbk", "utf-8//IGNORE", $content);
                $content = $this->trans->t2c($content);
                $content = ltrim($content, "var desc='");
                $content = str_replace("';", '', $content);
                $content = $this->filterDesc($content);
            }
        }
        return array('nick' => $nick, 'pic_url' => $pic_url, 'content' => $content);
    }

    /**
     * 天猫抓取规则
     * @param unknown $c
     * @return multitype:Ambigous <string, unknown> Ambigous <multitype:, unknown> string
     */
    private function getTmallItem($c) {
        $nick = '';
        preg_match('/<input type="hidden" name="seller_nickname" value="(.*?)" \/>/i', $c, $a);
        if (isset($a[1])) {
            $nick = $a[1];
        }
        $pic_url = array();
        preg_match_all('/<a href="#"><img src="((.*?)_60x60(.*?)\.jpg)" \/><\/a>/i', $c, $a);
        if (isset($a[2]))
            $pic_url = $a[2];
        $content = '';
        $is_desc = '1';
        if ($is_desc == 1) {
            preg_match('/"descUrl":"(.*?)"/i', $c, $a);
            if (isset($a[1])) {
                if(substr($a[1],0,2) == "//"){
                    $a[1] = "http://".substr($a[1],2);
                }
                $content = $this::getUrlContent($a[1]);
                $content = @iconv("gbk", "utf-8//IGNORE", $content);
                $content = $this->trans->t2c($content);
                $content = ltrim($content, "var desc='");
                $content = str_replace("';", '', $content);
                $content = $this->filterDesc($content);
            }
        }
        return array('nick' => $nick, 'pic_url' => $pic_url, 'content' => $content);
    }

    /**
     * unicode编码中文转换
     * @param unknown $str
     */
    public function unescapeCh($str) {
        $str = rawurldecode($str);
        preg_match_all("/(?:%u.{4})|&#x.{4};|&#\d+;|.+/U", $str, $r);
        $ar = $r[0];
        //print_r($ar);
        foreach ($ar as $k => $v) {
            if (substr($v, 0, 2) == "%u") {
                $ar[$k] = iconv("UCS-2BE", "UTF-8", pack("H4", substr($v, -4)));
            } elseif (substr($v, 0, 3) == "&#x") {
                $ar[$k] = iconv("UCS-2BE", "UTF-8", pack("H4", substr($v, 3, -1)));
            } elseif (substr($v, 0, 2) == "&#") {

                $ar[$k] = iconv("UCS-2BE", "UTF-8", pack("n", substr($v, 2, -1)));
            }
        }
        return join("", $ar);
    }

    /**
     * 过滤
     * @param unknown $document
     * @return string
     */
    private function filterDesc($document) {
        $document = trim($document);
        if (!$document) {
            return $document;
        }
        $search = array("'<script[^>]*?>.*?</script>'si", "'<a[^>]*?>'si", "'<\/a>'si");
        $replace = array('', '', '');
        return @preg_replace($search, $replace, $document);
    }

    /**
     * 获取远程页面内容
     * @param unknown $url
     * @return string 页面内容
     */
    public static function getUrlContent($url) {

        if (function_exists('curl_init')) {
            $header = array();
            $aurl = parse_url($url);
            $header[] = 'Accept:text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8';
            $header[] = 'Accept-Language: zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3';
            $header[] = 'User-Agent: Mozilla/5.0 (Windows NT 5.2; rv:25.0) Gecko/20100101 Firefox/25.0';
            $header[] = 'Host: ' . $aurl['host'];
            $header[] = 'Connection: Keep-Alive';
            $header[] = 'Cookie: tracknick=\u590F\u6587\u8F69';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FAILONERROR, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            @curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            @curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
            @curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 35);
            curl_setopt($ch, CURLOPT_TIMEOUT, 40);

            if ($_SERVER['HTTP_HOST'] == LIVE_PROXY_DOMAIN) {
                curl_setopt($ch, CURLOPT_PROXY, LIVE_PROXY);
            }
            $pageContent = @curl_exec($ch);
            $info = @curl_getinfo($ch);
            curl_close($ch);
            if ($pageContent && isset($info['http_code']) && $info['http_code'] == '200') {
                return $pageContent;
            }
        }

        $snoopy = new Snoopy();
        $snoopy->agent = 'Mozilla/5.0 (Windows NT 5.2; rv:25.0) Gecko/20100101 Firefox/25.0';
        $snoopy->cookies['tracknick'] = '\u590F\u6587\u8F69';
        $snoopy->fetch($url);
        if ($pageContent = $snoopy->results) {
            return $pageContent;
        }
        return @file_get_contents($url);
    }

    /**
     * 获取内容
     * @param unknown $url
     * @param string $is_post
     * @param string $curlPost
     * @return string 页面内容
     */
    public static function getContent($url, $is_post = "false", $curlPost = "") {
// 		define('LIVE_PROXY','10.160.29.21:65002');
// 		define('LIVE_PROXY_DOMAIN','www.jinritemai.com');
        if (function_exists('curl_init')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FAILONERROR, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 35);
            curl_setopt($ch, CURLOPT_TIMEOUT, 40);

            if (strstr($url, 'https')) {
                curl_setopt($ch, CURLOPT_PORT, 443);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // https请求 不验证证书和hosts
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            }
            curl_setopt($ch, CURLOPT_POST, $is_post);
            if ($curlPost) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
            }
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            @curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            @curl_setopt($ch, CURLOPT_MAXREDIRS, 10);

            if ($_SERVER['HTTP_HOST'] == LIVE_PROXY_DOMAIN) {
                curl_setopt($ch, CURLOPT_PROXY, LIVE_PROXY);
            }
            $pageContent = @curl_exec($ch);
            curl_close($ch);
        } else {
            $pageContent = @file_get_contents($url);
        }
        return $pageContent;
    }

}
