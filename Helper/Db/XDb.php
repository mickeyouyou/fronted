<?php
namespace Helper\Db;

use General\Db\Sql\Predicate;
use Core\Functions;
use Core\Factory;
use Core\Common;

/**
 * 原有db功能太少，用此类扩充db 
 * 
 */
class XDb
{
    const INSERT_STEP = 5000;
    const UPDATE_STEP = 5000;

    /**
     * 多行插入
     * 
     * @param mixed $table 
     * @param mixed $lines 
     * @static
     * @access public
     * @return void
     */
    public static function mutiInsert($table, $lines)
    {
        if(count($lines) > 0)
        {
            $keys = array_keys(current($lines));
            $sql = "insert into $table(" . self::escapeArrayToString($keys, '') . ") values ";

            foreach(array_chunk($lines, self::INSERT_STEP, true) as $datas)
            {
                $valueStrings = array();
                foreach($datas as $data)
                {
                    $valueStrings[] = "(" . self::escapeArrayToString($data) . ")";
                }

                self::executeSql($table, $sql . implode(',', $valueStrings));
            }
        }
    }

    /**
     * 多行插入。只有一列是变量,其余列值相同
     * 
     * @param mixed $table 
     * @param mixed $common_colomns 
     * @param mixed $colomn_name 
     * @param mixed $colomn_values 
     * @static
     * @access public
     * @return void
     */
    public static function mutiInsertOfOneVars($table, $common_colomns, $colomn_name, $colomn_values)
    {
        $lines = array();
        foreach($colomn_values as $value)
        {
            $line = $common_colomns;
            $line[$colomn_name] = $value;
            $lines[] = $line;
        }

        self::mutiInsert($table, $lines);
    }

    /**
     * 批量更新 
     * 
     * @param mixed $table 完整表名
     * @param mixed $indexColumn 列名，唯一索引列，一般是主键列(不支持多个唯一索引的表)
     * @param mixed $updateColumn 列名，欲修改的列
     * @param Array $lines 数据
     * @static
     * @access public
     * @return array 成功修改的数据键值
     */
    public static function mutiUpdate($table, $indexColumn, $updateColumn, Array $datas)
    {
        $sucessIds = array();
        foreach(array_chunk($datas, self::UPDATE_STEP, true) as $lines)
        {
            $id2NewValue = array();
            if(!empty($lines))
            {
                $sql = "select $indexColumn from $table where $indexColumn in (" . self::escapeArrayToString(array_keys($lines)) . ") ";
                $result = self::executeSql($table, $sql)->getFetchArrays();

                foreach($result as $r)
                {
                    $id = $r[$indexColumn];
                    $id2NewValue[$id] = $lines[$id];
                }
            }

            if(!empty($id2NewValue))
            {
                $conditions = array();
                foreach($id2NewValue as $id => $value)
                {
                    $safeId = self::_escapeString($id);
                    $safeValue = self::_escapeString($value);
                    $conditions[] = "($safeId, $safeValue)";
                }

                $sql = "insert into $table($indexColumn, $updateColumn) value "
                    .implode(',', $conditions)
                    ." ON DUPLICATE KEY UPDATE $updateColumn=VALUES($updateColumn)";
                self::executeSql($table, $sql);

                $sucessIds = array_merge($sucessIds, array_keys($id2NewValue));
            }
        }

        return $sucessIds;
    }


    /**
     * 执行sql 
     * 
     * @param mixed $sql 
     * @param mixed $params 
     * @static
     * @access public
     * @return void
     */
    public static function executeSql($table, $sql, $params=array())
    {
        $newParams = array();
        foreach($params as $key => $param)
        {
            if(is_array($param))
            {
                $newKeys = array();
                foreach($param as $k => $v)
                {
                    $newKey = $key . '_' . $k;
                    $newKeys[] = $newKey;
                    $newParams[$newKey] = $v;
                }
                $sql = str_replace($key, implode(',', $newKeys), $sql);
            }
            else
            {
                $newParams[$key] = $param;
            }
        }

        $db = self::_getDb($table);
        $st = $db->createStatement($sql, $newParams);

        return $db->query($st);
    }

    /**
     * array绑定参数预查询效率太低，array可以用本函数进行转义
     * 
     * @param string $arr 
     * @static
     * @access public
     * @return string
     */
    public static function escapeArrayToString($arr, $border="'")
    {
        $safeArr = array();
        foreach($arr as $str)
        {
            $safeArr[] = self::_escapeString($str, $border);
        }

        return implode(',', $safeArr);
    }

    /**
     * 为保证sql安全，用次方法转义字符串里面的非法值 
     * 
     * @param string $str 
     * @static
     * @access private
     * @return string
     */
    private static function _escapeString($str, $border="'")
    {
        $str = str_replace("'", "\'", $str);
        if(get_magic_quotes_gpc())
        {
            $str = stripslashes($str);
        }

        return $border . $str . $border;
    }

    /**
     * 获取数据库连接 
     * 
     * @param mixed $table 
     * @static
     * @access private
     * @return void
     */
    private static function _getDb($table)
    {
        $originalTable = self::getSimbleTableName($table);
        $mapper = \Yaf\Registry::get('Mount')->get('Mapper');
        Functions::assertTure(isset($mapper[$originalTable]), "The Factory::table $originalTable Not Exist In DB Server Tables");
        $dbServer = $mapper[$originalTable];
        $user_adapter = Factory::db($dbServer);

        return $user_adapter;
    }

    /**
     * 统计系统表的完整表名 
     * 部分表按照日期分表
     * 
     * @param mixed $table 
     * @param mixed $day 
     * @static
     * @access public
     * @return void
     */
    public static function getFullTableNameMapByDay($table, $day = null)
    {
        if(is_null($day))
        {
            $fullTableName = Common::dbTableNames($table);
        }
        else
        {
            $mapper = \Yaf\Registry::get('Mount')->get('Mapper');
            Functions::assertTure(isset($mapper[$table]), 'The Common::dbTableNames Detected Table Not Exists');
            $DBConfig = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'database');

            list($year, $month, $day) = explode('-', $day);

            $fullTableName = $DBConfig->db_prefix . '_' . $year . '_' .$month . '.'
                . $DBConfig->table_prefix . $table . '_' . $year . '_' . $month . '_' . $day;
        }

        return $fullTableName;
    }

    /**
     * 根据完整表名(包括数据库名、分表号)获取简单表名 
     * 
     * @param mixed $fullTableName 
     * @static
     * @access private
     * @return void
     */
    public static function getSimbleTableName($fullTableName)
    {
        $arr = explode('.', $fullTableName);
        $table = end($arr);

        return preg_replace(array('/^t_/', '/_\d+/'), '', $table);
    }

    /**
     * 批量插入一个单位的数据 
     * 
     * @param string $table 
     * @param string $sql 
     * @param string $lines 
     * @static
     * @access private
     * @return void
     */
    private static function _unitInsert($table, $sql, $lines)
    {
        $params = array();
        foreach($lines as $k => $l)
        {
            $values = array();
            foreach($l as $colomn => $value)
            {
                $bindKey = ":_{$k}_{$colomn}";
                $values[] = $bindKey;
                $params[$bindKey] = $value;
            }
            $value_str = implode(',', $values);
            $sql .= "($value_str), ";
        }
        $sql = substr($sql, 0, strlen($sql)-2);

    }

}
