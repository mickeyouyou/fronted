<?php
/**
 * 读取ini配置节点，导出相应的对象
 *
 * @author: xudianyang
 * @version: Common.php v-1.0.0, 2014-08-17 21:37 Created
 * @copyright Copyright (c) 2014 Beijing Jinritemai Technology Co.,Ltd. (http://www.jinritemai.com)
 */
namespace Core;

use Exception;
use Helper\Image\Image;

final class Common
{
    /**
     * Session Csrf Token生成
     *
     * @return string
     * @throws \Exception\RuntimeException
     */
    static public function getSessionCsrfToken()
    {
        $csrf  = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'csrf');
        if (!$csrf instanceof \Yaf\Config\Ini) {
            throw new Exception\RuntimeException('The Common::getSessionCsrfToken need default csrf config');
        }
        $token    = uniqid("", true);
        $tokens   = Factory::session()->get($csrf->namespace);
        $counting = (int)Factory::session()->get($csrf->namespace . ':count');
        $counting +=1;
        $i        = $counting % $csrf->max;
        $tokens[$i] = $token;
        Factory::session()->set($csrf->namespace . ':count', $counting);
        Factory::session()->set($csrf->namespace, $tokens);

        return $token;
    }

    /**
     * Session Csrf Token Name 返回
     *
     * @return string|\Yaf\Config_Abstract
     * @throws \Exception\RuntimeException
     */
    static public function getSessionCsrfName()
    {
        $csrf  = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'csrf');
        if (!$csrf instanceof \Yaf\Config\Ini) {
            throw new Exception\RuntimeException('The Common::getSessionCsrfName need default csrf config');
        }

        $name = $csrf->name;

        if (!$name) $name = 'defaultCsrfName';
        return $name;
    }

    /**
     * 验证是否正常的请求（防止csrf）
     * 
     * @param $token
     * @return bool
     * @throws \Exception\RuntimeException
     */
    static public function checkSessionCsrfIsValid($token)
    {
        $csrf  = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'csrf');
        if (!$csrf instanceof \Yaf\Config\Ini) {
            throw new Exception\RuntimeException('The Common::checkSessionCsrfIsValid need default csrf config');
        }

        $tokens = Factory::session()->get($csrf->namespace);
        if (is_array($tokens) && in_array($token, $tokens)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 根据当前使用的route协议生成URL
     *
     * @param $route
     * @param array $params
     * @return string
     * @throws \Exception\RuntimeException
     */
    static public function createUrl($route, $params = array())
    {
        $route = ltrim($route, '/');
        if ($route == '') {
            $route = array('index', 'index', 'index');
        } else {
            $route = explode('/', $route);
        }
        $mvc   = array();
        $num   = count($route);

        if ($num <= 0 || $num > 3) {
            throw new Exception\RuntimeException('Common::createUrl failed');
        }

        switch($num) {
            case 1:
                $mvc[':a'] = $route[0];
                break;

            case 2:
                $mvc[':a'] = $route[1];
                $mvc[':c'] = $route[0];
                break;

            case 3:
                $mvc[':a'] = $route[2];
                $mvc[':c'] = $route[1];
                $mvc[':m'] = $route[0];
                break;
        }

        $router = \Yaf\Dispatcher::getInstance()->getRouter();
        $routeName = $router->getCurrentRoute();
        $routeObject = $router->getRoute($routeName);
        $url = $routeObject->assemble($mvc, $params);

        return rtrim(APP_URL, DIRECTORY_SEPARATOR) . $url;
    }

    static private function getTableInfoBySplitId($id)
    {
        settype($id, 'string');
        $id    = sprintf("%03s", $id);

        $dbIndex = substr($id, -3, 2);
        $tableIndex = substr($id, -1, 1);

        return array($dbIndex, $tableIndex);
    }

    /**
     * 返回完整的全表名
     *
     * @param $table
     * @param null $id
     * @return string
     * @throws \Exception\RuntimeException
     */
    static public function dbTableNames($table, $id = null)
    {
        $mapper = \Yaf\Registry::get('Mount')->get('Mapper');
        if (!isset($mapper[$table])) {
            throw new Exception\RuntimeException('The Common::dbTableNames Detected Table Not Exists');
        }

        $DBConfig = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'database');

        if (is_null($id)) {
            return $DBConfig->db_prefix . '.' . $DBConfig->table_prefix . $table ;
        }

        list($dbIndex, $tableIndex) = self::getTableInfoBySplitId($id);

        return $DBConfig->db_prefix . '_' . $dbIndex . '.' .
        $DBConfig->table_prefix . $table . '_' . $dbIndex . '_' . $tableIndex;
    }

    /**
     * 把id数组根据表分组 
     * 
     * @param mixed $ids 
     * @static
     * @access public
     * @return array
     */
    static public function groupIdByTable($ids)
    {
        $groupedIds = array();
        foreach($ids as $id)
        {
            list($dbIndex, $tableIndex) = self::getTableInfoBySplitId($id);
            $tableId = $dbIndex . $tableIndex;

            if(false == isset($groupedIds[$tableId]))
            {
                $groupedIds[$tableId] = array();
            }
            $groupedIds[$tableId][] = $id;
        }

        return $groupedIds;
    }

    /**
     * 返回资源文件唯一key
     *
     * @param string $filename 资源文件名
     * @param string $ext 资源文件扩展名
     * @return string
     */
    static public function getStorageKey($filename, $ext)
    {
        $ext = trim($ext, '.');
        $fileConfig = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'file');
        $bucket = $fileConfig->bucket;
        if (empty($_SERVER['SERVER_ADDR'])) {
            $_SERVER['SERVER_ADDR'] = microtime();
        }

        $mtime  = explode(' ',  microtime());
        $mtime  = $mtime[1] . str_replace('0.', '', $mtime[0]);
        $key = $bucket . '/' . date('Y/md') . '/' .
               str_replace('.', '', md5($_SERVER['SERVER_ADDR'] . uniqid($bucket . '-'. $filename, true))) .
               '-' . $mtime . '.' . $ext;

        return $key;
    }

    /**
     * 完成附件的上传并生成附件的储存key
     *
     * @param boolean $cloud 是否上传到七牛云储存，默认为true表示上传
     * @return array
     * @throws \Exception\RuntimeException
     */
    static public function uploadAttachmentReturnKeys($cloud = true) {
        $fileFields = \Yaf\Dispatcher::getInstance()->getRequest()->getFiles();
        if (empty($fileFields)) {
            return array();
        }
        $returnKeys = array();

        $fileConfig = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'file');
        $folder = new \General\File\Folder();
        $qiniu = \Core\Factory::file('qiniu');
        foreach ($fileFields as $name => $fileField) {
            if (is_string($fileField['tmp_name'])) {
                if (!is_uploaded_file($fileField['tmp_name'])) {
                    throw new Exception\RuntimeException('The Common::uploadAttachmentReturnKeys Detected Dangerous');
                }

                $ext = strrchr($fileField['name'], '.');
                $key = self::getStorageKey($fileField['tmp_name'], $ext);
                $returnKeys[$name] = $key;
                $saveFileName = rtrim($fileConfig->base_path, '/') . '/' . $key;
                $savePath     = dirname($saveFileName);
                $folder->write($savePath);
                if (!move_uploaded_file($fileField['tmp_name'], $saveFileName)) {
                    throw new Exception\RuntimeException(sprintf(
                        'The Common::uploadAttachmentReturnKeys Can Not Move Uploaded File: %s => $%s'
                    ), $fileField['tmp_name'], $saveFileName);
                }

                if ($cloud) {
                    try {
                        $qiniu->write($key, $saveFileName);
                    } catch (\Exception $e) {
                        $msg = $e->getMessage();
                        \Yaf\Registry::get('Mount')->get('Logger')->log('error', var_export($msg, true));
                        throw new Exception\RuntimeException(sprintf(
                            'The Common::uploadAttachmentReturnKeys Call Qiniu Cloud Storage Error: %s'
                        ), $msg);
                    }
                }
                continue;
            }

            foreach ($fileField['tmp_name'] as $k => $tmp_name) {
                if (!is_uploaded_file($tmp_name)) {
                    throw new Exception\RuntimeException('The Common::uploadAttachmentReturnKeys Detected Dangerous');
                }

                $ext = strrchr($fileField['name'][$k], '.');
                $key = self::getStorageKey($tmp_name, $ext);
                $returnKeys[$name][] = $key;
                $saveFileName = rtrim($fileConfig->base_path, '/') . '/' . $key;
                $savePath     = dirname($saveFileName);
                $folder->write($savePath);
                if (!move_uploaded_file($tmp_name, $saveFileName)) {
                    throw new Exception\RuntimeException(sprintf(
                        'The Common::uploadAttachmentReturnKeys Can Not Move Uploaded File: %s => $%s'
                    ), $tmp_name, $saveFileName);
                }

                if ($cloud) {
                    try {
                        $qiniu->write($key, $saveFileName);
                    } catch (\Exception $e) {
                        $msg = $e->getMessage();
                        throw new Exception\RuntimeException(sprintf(
                            'The Common::uploadAttachmentReturnKeys Call Qiniu Cloud Storage Error: %s'
                        ), $msg);
                    }
                }
            }
        }

        return $returnKeys;
    }

    /**
     * 获取本地储存文件的全路径
     *
     * @param string $key 文件的key
     * @return string
     */
    static public function getLocalAttachmentPathName($key)
    {
        $fileConfig = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'file');
        $fileName = rtrim($fileConfig->base_path, '/') . '/' . $key;

        return $fileName;
    }

    /**
     * 获取可访问的七牛云储存url
     *
     * @param string $key 文件的key
     * @return mixed
     */
    static public function getQiniuCloudUrl($key) {
        $qiniu = \Core\Factory::file('qiniu');
        return $qiniu->read($key);
    }

    /**
     * 获取缩略url
     * 
     * @param mixed $location 
     * @param mixed $width 
     * @param mixed $height 
     * @static
     * @access public
     * @return string
     */
    static public function getThumbUrl($location, $width, $height)
    {
        $qiniu = \Core\Factory::file('qiniu');
        return $qiniu->imageThumb($location, '0', $width, $height);
    }

    /**
     * 返回资源文件的域名（考虑CDN）
     * 注意非线下环境使用当前应用域名
     *
     * @return string
     */
    static public function getResDomain()
    {
        if (ini_get('yaf.environ') != 'online') {
            return rtrim(APP_URL, '/') . '/';
        }

        $domains   = \Yaf\Application::app()->getConfig()->application->cdn_domains->toArray();
        $domainKey = array_rand($domains);
        $domain = $domains[$domainKey];

        if (strpos($domain, 'http://') === false) {
            $domain = 'http://' . rtrim($domain, '/') . '/';
        } else {
            $domain = rtrim($domain, '/') . '/';
        }

        return $domain;
    }

    /**
     * 获取资源文件完整url
     * 
     * @param mixed $file 
     * @static
     * @access public
     * @return string
     */
    public static function getResourceUrl($file)
    {
        $fullUrl = $file;
        if('upload1/' == substr($file, 0, 8) || preg_match('/^original_/', $file))
        {
            $fullUrl = self::getQiniuCloudUrl($file);
        }
        else if('assets/' == substr($file, 0, 7))
        {
            $fullUrl = self::getResDomain() . $file . STATIC_VERSION; 
        }

        return $fullUrl;
    }

    /**
     * 生成缩略图
     *
     * @param string $img     原始图片
     * @param int $width      缩略图宽
     * @param int $height     缩略图高
     * @param mixed $cut      new! 是否采用裁剪算法，如果为 int，则该值为新算法中对应的裁剪位置, null为不使用
     * @param int $quality    缩略图质量
     * @return null|string    缩略图地址
     */
    static public function thumb($img, $width, $height, $cut = 1, $quality = null)
    {
        $fileConfig = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'file');
        $basePath   = $fileConfig->base_path;
        $basename   = basename($img);
        $targetImg  = dirname($img) . '/thumb_' . $width . '_' . $height . '_' . $basename;
        $absoluteTargetImg = rtrim($basePath, '/') . '/' . $targetImg;
        $absoluteSourceImg = rtrim($basePath, '/') . '/' .$img;;

        if (!file_exists($absoluteTargetImg) ||
            filemtime($absoluteTargetImg) < filemtime($absoluteSourceImg)) {
            $image = new Image();
            if (is_null($quality)) {
                $image->set_thumb($width, $height);
            } else {
                $image->set_thumb($width, $height, $quality);
            }

            if (!is_null($cut)) {
                $returnImg = $image->thumb_cut($absoluteSourceImg, $absoluteTargetImg, is_int($cut) ? $cut : 0, true)
                                ? $absoluteTargetImg
                                : $absoluteSourceImg;
            } else {
                $returnImg = $image->thumb($absoluteSourceImg, $absoluteTargetImg)
                                ? $absoluteTargetImg
                                : $absoluteSourceImg;
            }
        } else {
            $returnImg = $absoluteTargetImg;
        }

        return ltrim(str_replace($basePath, '', $returnImg), '/');
    }

    /**
     * 产生全局唯一ID
     *
     * @param string $type 返回ID的类型，必须
     * @param string $id 分表规则的ID，可选默认为null
     * @param int $count 返回的ID数量，可选默认为1
     * @return mixed
     * @throws \Exception
     */
    static public function generatedID($type, $id = null, $count = 1)
    {
        try {
            $transfer = \Core\Factory::transfer();
            $transfer->setOptions(
                array(
                    'router' => 'generator#index/index/index'
                )
            );

            $count = (int) $count;
            if (!$count) {
                $count = 1;
            }

            if (!is_null($id)) {
                $id     = sprintf("%03s", $id);
                $suffix = substr($id, -3);
            } else {
                $suffix = '';
            }

            $idInfo = $transfer->api(
                array(
                    'type'   => $type,
                    'count'  => $count,
                    'suffix' => $suffix,
                )
            );

            if ($count == 1) {
                return $idInfo['0'];
            } else {
                return $idInfo;
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * 调用远程接口的封装，重试机制及日志记录
     *
     * @param string $api api标识如，order#index/payment/getmodeofpayment
     * @param array $params 接口请求参数
     * @param int $times 接口重试次数
     * @return mixed array('errno' => '状态码', 'message' => '调用信息', 'data' => '数据')
     * @throws \Exception
     */
    static public function callApi($api, $params = array(), $times = 1) {
        $i = $times;
        while ($i > 0) {
            try {
                $transfer = \Core\Factory::transfer();
                $transfer->setOptions(array('router' => $api));
                $result = $transfer->api($params);
                break;
            } catch (Exception $e) {
                $msg  = '\Core\Common::callApi()调用远程API出错' . "\r\n";
                $msg .= $e->getMessage() . "\r\n";
                $msg .= 'API_ID: ' . $api . "\r\n";
                $msg .= 'PARAMS: ' . var_export($params, true) . "\r\n";
                $msg .= 'Times: ' . $times;
                if ($i > 1) {
                    $logger = \Yaf\Registry::get('Mount')->get('Logger');
                    $logger->log('error', $msg);
                } else {
                    throw $e;
                }
            }
            $i--;
        }

        return $result;
    }

    /**
     * 根据商品id 获取所在的表
     * @param string $productId
     * @return array
     */
    static public function getDataTableNameByProductId($productId){
        $type = substr($productId, 2, 3);
        $tableSuffix = '';
        switch($type){
            case '001':
                $tableSuffix = 'product';
                break;
            case '011':
                $tableSuffix = 'single_product';
                break;
            case '012':
                $tableSuffix = 'outer_product';
                break;
            default:
                $tableSuffix = 'no_suffix';
                break;
        }

        if($tableSuffix == 'no_suffix'){
            $returnData = array(
                'type' => 'no_suffix',
                'tableName' => ''
            );
        }else{
            $returnData = array(
                'tableName' => $type == '011'
                        ? self::dbTableNames($tableSuffix)
                        : self::dbTableNames($tableSuffix, $productId),
                'type' => $tableSuffix
            );
        }

        return $returnData;
    }

    /**
     * 验证头条RSA签名（防伪）
     *
     * @param array $data 头条请求传递的数据
     * @return boolean
     */
    static public function verifyTTRSASign(array $data)
    {
        $sign = $data['tt_sign'];
        unset($data['tt_sign']);
        unset($data['tt_sign_type']);
        ksort($data);

        $message = '';
        foreach ($data as $key => $value) {
            $message .= $key . '=' . $value . '&';
        }
        $message = substr($message, 0, -1);

        $publicKey = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'rsa')['toutiao_public_key'];
        $publicKeyId = openssl_pkey_get_public($publicKey);
        $result = openssl_verify($message, base64_decode($sign), $publicKeyId);
        openssl_free_key($publicKeyId);

        return $result;
    }

    /**
     * RSA签名数据
     *
     * @param array $data 待签名数据
     * @return string
     */
    static public function jrtmRSASign(array $data)
    {
        ksort($data);
        $Keys = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'rsa');

        $message = '';
        foreach ($data as $key => $value) {
            $message .= $key . '=' . $value . '&';
        }

        $message = substr($message, 0, -1);
        $privateKeyId = openssl_pkey_get_private($Keys['jrtm_private_key']);
        $signature = '';
        openssl_sign($message, $signature, $privateKeyId);
        openssl_free_key($privateKeyId);

        return base64_encode($signature);
    }

    //获取客户端设备的类型
    static function getDeviceType(){
        $agent = strtolower($_SERVER['HTTP_USER_AGENT']);
        $type = 'other';
        $version = '';
        $room = '';
        if(strpos($agent, 'iphone') || strpos($agent, 'ipad')){
            $type = 'ios';
        }
        if(strpos($agent, 'android')){
            $type = 'android';
            //$preg_match = "/;(\s*)(\S*?\s?\S*?)(\s*Build)/i";
            $preg_match = "/;(\s*)([A-Za-z0-9_-]*?\s?[A-Za-z0-9_-]*?)(\s*Build\/)(\S*?)\)/";
            preg_match($preg_match, $_SERVER['HTTP_USER_AGENT'],$matches);
            $version = empty($matches[2]) ?  '' : $matches[2];
            $room = empty($matches[4]) ? '' : $matches[4];
        }
        return array(
            'type' => $type,
            'version' => $version,
            'room' => $room
        );
    }


    //判断当前登陆的设备及用户信息，是否支持支付功能
    static function getSupportPay(){
        $isSupportPay = \Core\Factory::session()->get('is_support_pay');
        $userType = \Core\Factory::session()->get('uid_type');
        $jsBridgeLogin = \Core\Factory::session()->get('jsbridge_login');
        $os = \Core\Factory::session()->get('os');


        if($os == 'android'){
            if($jsBridgeLogin == 1){
                $isSupport = 1;
            }else{
                if($userType == '12' && $isSupportPay == 1){
                    $isSupport = 1;
                }else{
                    $isSupport = 0;
                }
            }
        }
        if($os == 'ios'){
            if($userType == '12' && $isSupportPay ==1){
                $isSupport = 1;
            }else{
                $isSupport = 0;
            }
        }

        if($isSupport == 1){
            $now = time()+3600*12;
            \Core\Factory::session()->set("order_login", $now);
        }else{
            $orderLogin = \Core\Factory::session()->get("order_login");
            if ($orderLogin) {
                \Core\Factory::session()->del('order_login');
                \Core\Factory::cookie()->set('shoppingcart', null);
            }
        }
        return array('type' => $os,'is_support'=>$isSupport);
    }

    /**
     * 图片质量控制
     * @param $url 图片url
     * @param $configSize   图片标准size
     * @param $online   是否是外网，是为1，否为0，默认为本地文件
     * @return int
     */
    static function getQuality($url,$configSize,$online = 0){
        static $i = 0;
        if ($i++ > 6){
            exit;
        }

        $imgSize = "";
        if($online == 1){

            $url = parse_url($url);

            if ($fp = @fsockopen($url['host'], empty($url['port']) ? 80 : $url['port'], $error)) {
                fputs($fp, "GET " . (empty($url['path']) ? '/' : $url['path']) . " HTTP/1.1\r\n");
                fputs($fp, "Host:$url[host]\r\n\r\n");
                while(!feof($fp)) {
                    $tmp = fgets($fp);
                    //if (isset($arr[1])) {
                    //print_r($arr);
                    //}
                    if (trim($tmp) == '') {
                        break;
                    } else if (preg_match('/location:\s(.*)/si', $tmp, $arr)) {
                        @fclose($fp);
                        return \Core\Common::getQuality(trim($arr[1]),$configSize,$online);
                    } else if (preg_match('/Content-Length:\s(.*)/si', $tmp, $arr)) {
                        @fclose($fp);
                        $imgSize = trim($arr[1]);
                        break;
                    }
                }
                @fclose($fp);
            }
            $imgSize = ceil($imgSize / 1024);
        }else{
            $imgSize = ceil(filesize($url) / 1024);
        }

        if($url){
            $imgSpace = ceil(filesize($url) / 1024);
            $srate = floor(100*40/$imgSpace);
        }else{
            $srate = 100;
        }

        if(isset($imgSize) && $imgSize!= ""){
            $rate = floor(100*$configSize/$imgSize);
            if($rate > 90){
                if($srate > 50){
                    $quality = 95;
                }elseif($srate > 20){
                    $quality = 90;
                }elseif($srate > 10){
                    $quality = 75;
                }
            }elseif($rate > 80){
                $quality = 92;
            }elseif($rate > 70){
                $quality = 90;
            }elseif($rate > 60){
                $quality = 88;
            }elseif($rate > 20){
                $quality = 86;
            }elseif($rate > 15){
                if($srate > 20){
                    $quality = 76;
                }elseif($srate > 15){
                    $quality = 74;
                }elseif($srate > 10){
                    $quality = 70;
                }
            }elseif($rate > 5){
                if($srate > 20){
                    $quality = 74;
                }elseif($srate > 15){
                    $quality = 70;
                }elseif($srate > 10){
                    $quality = 65;
                }
            }else{
                if($srate > 20){
                    $quality = 70;
                }elseif($srate > 15){
                    $quality = 69;
                }elseif($srate > 10){
                    $quality = 68;
                }
            }
        }else{
            $quality = 85;
        }
        return $quality;
    }

    /**
     * 商品详情添加头部信息
     * @param $detail
     * @return string
     */
    static function addMateForDetail($detail){

        $metaStr = '<meta http-equiv="Content-Type" content-type="text/html" charset="UTF-8" />';
        $styleStr = "<style>body{width:790px;background: #fff;}</style>";
        $bodyStart = "<body><div style="."margin:0 auto;width:100%;".">";
        $bodyEnd = "</div></body>";

        if (!strstr($detail,"<body") && !strstr($detail,"<meta")) {
            $detail = $bodyStart.$detail;
        } if(!strstr($detail,"<style")){
            $detail = $styleStr.$detail;
        } if(!strstr($detail,"<meta")){
            $detail = $metaStr.$detail;
        }
        $detail .= $bodyEnd;
        return $detail;
    }

    /**
     * 切图入队列
     *
     * @param array $params 切图队列参数
     * @return int $jobId
     */
    static function imageCutQueue($params)
    {
        // 区分运营和商户
        if (@$params['product_flag'] == 'single_shop' || @$params['product_flag'] == 'activity_shop') {
            $app_name = 'Shopec';
            $jobBody = array(
                'product_id'   => $params['product_id'],
                'product_flag' => $params['product_flag'],
                'shop_id'      => $params['shop_id'],
            );
        } else {
            $app_name = APP_NAME;
            $jobBody = array(
                'product_id'   => $params['product_id'],
                'product_flag' => $params['product_flag'],
            );
        }

        // 获取待执行的任务
        $tableNameInfo = \Core\Common::getDataTableNameByProductId($params['product_id']);
        $table = \Core\Factory::sql($tableNameInfo['tableName']);
        $queueInfo = $table->select()
            ->columns('queueid')
            ->where(array('product_id' => $params['product_id']))
            ->limit(1)
            ->execute()
            ->current();

        $lastQueueId = $queueInfo['queueid'];
        $body = json_encode($jobBody);
        $beanstalk = \Core\Factory::beanstalk();
        $beanstalk->useTube($app_name);

        // 如果上次的任务未执行，就删除任务
        if ($lastQueueId) {
            try {
                $job = new \General\Pheanstalk\Job($lastQueueId, $body);
                $beanstalk->delete($job);
            } catch (Exception $e) {
                // 无须处理
            }
        }

        try {
            $body = json_encode($params);
            $beanstalk = \Core\Factory::beanstalk();
            $beanstalk->useTube($app_name);
            $jobId = $beanstalk->put($body, 1024, 0, 600);
            $data = array(
                'queueid' => $jobId,
            );

            $table->update()->set($data)->where(array('product_id' => $params['product_id']))->execute();
        } catch (\Exception $e) {
            $code    = $e->getCode();
            $message = $e->getMessage();

            $msg =  $message . "：\r\n" . "Uri: "     . @$_SERVER['REQUEST_URI'] . "\r\n" .
                    "File: "    . $e->getFile() . "\r\n" .
                    "Line: "    . $e->getLine() . "\r\n" .
                    "Code: "    . $code . "\r\n" .
                    "Message: " . $message . "\r\n" .
                    "Trace: "   . "\r\n" . $e->getTraceAsString() . "\r\n" .
                    "Query: "   . var_export(Application::app()->getDispatcher()->getRequest()->getQuery(), true) . "\r\n" .
                    "POST: "    . var_export(Application::app()->getDispatcher()->getRequest()->getPost(), true) . "\r\n" .
                    "PARAMS: "  . var_export(Application::app()->getDispatcher()->getRequest()->getParams(), true) . "\r\n";

            $logger = \Yaf\Registry::get('Mount')->get('Logger');
            /**
             * @var $logger \General\Psr\Log\AbstractLogger
             */
            $logger->log("error", $msg);

            $jobId = 0;
        }

        return $jobId;
    }


    /**
     * 根据用户手机端类型，判断前端加载下一页的方式 和 订单提交页面判断是否吸底
     * @return string
     */
    static function getMoreFuncStyle(){
        $deviceInfo = self::getDeviceType();
        $getMoreStyle = 'slip';
        $isFix = 1;//吸底
        $SpecialPhoneVersionData = \Yaf\Registry::get('Mount')->get('ConfigLoader','SpecialPhoneVersion')->toArray();
        if(!empty($SpecialPhoneVersionData) && $deviceInfo['version'] != '' && in_array($deviceInfo['version'],$SpecialPhoneVersionData['phone'])){
            $getMoreStyle =  'click';
            if($deviceInfo['room'] != '' && in_array($deviceInfo['room'],$SpecialPhoneVersionData['phoneRoom'])){
                $isFix = 2;//非吸底
            }
        }
        return array('getMoreStyle' => $getMoreStyle,'isFix'=>$isFix);
    }

    /**
     * 获取ip地址
     * @return mixed
     */
    static function getIP() {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $realip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $realip = $_SERVER['HTTP_CLIENT_IP'];
        } else {
            $realip = $_SERVER['REMOTE_ADDR'];
        }
        return $realip;
    }


    /**
     *
     * 校验系统图片验证码是否正确（支持多类型验证码）
     *
     * @param string $type 验证码类型，如立即购买时的验证码为buynow
     * @param string $phrase 从表单获取的待验证的验证码字符
     * @param boolean $step 是否为分步验证，默认为false
     * @return int 0代表验证通过，其他为异常
     */
    public static function checkSeccode($type, $phrase, $step = false)
    {
        // 验证码配置
        $config = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'seccode')->get($type);
        if (empty($config)) {
            $error = 1;
            goto returnBack;
        }

        $timesKey = session_id() . '_' . $config['name'] . '_times';
        $errorTimes = \Core\Factory::redis()->increment($timesKey, 1);

        if (($errorTimes - 1) > $config['times']) {
            // 超过尝试最大次数
            $error = 2;
            goto returnBack;
        }

        if (empty($phrase)) {
            // 验证码为空
            $error = 3;
            goto returnBack;
        }

        if (strlen($phrase) != $config['length']) {
            // 验证码长度不对
            $error = 4;
            goto returnBack;
        }

        $timeout = \Core\Factory::session()->get($config['name'] . '_timeout');
        if (time() > $timeout) {
            // 验证码码已过期
            $error = 5;
            goto returnBack;
        }

        if (strtolower($phrase) != strtolower(\Core\Factory::session()->get($config['name']))) {
            // 验证码不正确
            $error = 6;
            goto returnBack;
        } else {
            // 验证码正确，重置为空
            \Core\Factory::session()->del($config['name']);
            \Core\Factory::session()->del($config['name'] . '_timeout');
            \Core\Factory::redis()->remove($timesKey);
            $error = 0;
            goto returnBack;
        }

        returnBack:
            if ($step) {
                if ($error) {
                    \Core\Factory::session()->set($config['name'] . '_validate', 0);
                } else {
                    \Core\Factory::session()->set($config['name'] . '_validate', 1);
                }
            }

        return $error;
    }

    /**
     * 第二步验证图片验证码
     *
     * @param string $type 图片验证类型
     * @param boolean $delete 是否删除验证凭据
     * @return bool
     */
    public static function checkSeccodeAgain($type, $delete = false)
    {
        $config = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'seccode')->get($type);
        if (empty($config)) {
            return false;
        }

        if (\Core\Factory::session()->get($config['name'] . '_validate')) {
            if ($delete) {
                \Core\Factory::session()->del($config['name'] . '_validate');
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * 判断是否需要验证码
     *
     * @return bool
     */
    public static function requireSeccode()
    {
        $userId = \Core\Factory::session()->get('shop_user_id');
        $redisConfig = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'seccodeRedis')->toArray();
        // 强制要求验证码
        if (defined('SECCODE_FORCE') && SECCODE_FORCE == 1) {
            return true;
        }

        if (\Core\Factory::redis($redisConfig)->has($userId)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 在一定时间内访问库存（返回库存不足）的计数，并访问库存不足超过最大次数，则要求验证码
     *
     * @param int $userId 用户ID
     * @param string $productId 商品ID
     */
    public static function accessStockCounter($userId, $productId)
    {
        $key = $userId . ":" . $productId;
        \Core\Factory::redis()->getResource()->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_NONE);
        if (\Core\Factory::redis()->setnx($key, 1)) {
            \Core\Factory::redis()->setTimeout($key, STOCK_ERRNO_INTERVAL);
        } else {
            if (\Core\Factory::redis()->increment($key, 1) > STOCK_ERRNO_TIMES) {
                $redisConfig = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'seccodeRedis')->toArray();
                \Core\Factory::redis($redisConfig)->set($userId, 1, STOCK_SECCODE_TIME);
            }
        }
    }

    /**
     * @return string order_id
     */
    static public function generateOrderId()
    {
        $redis       = Factory::redis();
        $isExist     = $redis->get('unique_id');
        $lastOrderId = $redis->get('last_order_id');
        $today       = date('ymd');

        // todo 开始就检查是否是同一天的订单
        // 1 唯一ID自增
        if(empty($isExist)) {
            $redis->INCR('unique_id',  ORDER_START_ID);
        } else {
            // 字符串
            $redis->INCR('unique_id');
        }

        // 获取增长之后的id
        $uniqueId = $redis->get('unique_id');

        // 生成并记录订单ID
        if(empty($lastOrderId)) {
            // 重置UID, 上一个订单不存在 也视为不正常情况
            $redis->DELETE('unique_id');
            $redis->INCR('unique_id',  ORDER_START_ID);
            $newUid =  $redis->get('unique_id');

            $orderId = $today . str_pad($newUid, ORDER_AID_LENGTH, '0', STR_PAD_LEFT);
            $redis->SETNX('last_order_id', $orderId);
        } else {
            if($lastOrderId && substr($lastOrderId, 0, 6) == $today) {
                $orderId = $today . str_pad($uniqueId, ORDER_AID_LENGTH, '0', STR_PAD_LEFT);
                $redis->DELETE('last_order_id');
                $redis->SETNX('last_order_id', $orderId);
            } else {
                // 重置UID
                $redis->DELETE('unique_id');
                $redis->INCR('unique_id',  ORDER_START_ID);
                $newUid =  $redis->get('unique_id');
                $orderId = $today . str_pad($newUid, ORDER_AID_LENGTH, '0', STR_PAD_LEFT);
                $redis->DELETE('last_order_id');
                $redis->SETNX('last_order_id', $orderId);
            }
        }

        return $orderId;
    }

}
