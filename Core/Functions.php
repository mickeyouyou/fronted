<?php
/**
 *
 * @author
 * @copyright Beijing Jinritemai Technology Co.,Ltd.
 */

namespace Core;

use Exception\AssertException;

abstract class Functions
{
    static function remoteAddr($strict = false)
    {
        static $remote_addr = false;

        if ($remote_addr === false) {
            $keys = array(
                'HTTP_CLIENT_IP',
                'HTTP_X_FORWARDED_FOR',
                'HTTP_X_FORWARDED',
                'HTTP_X_CLUSTER_CLIENT_IP',
                'HTTP_FORWARDED_FOR',
                'HTTP_FORWARDED',
                'REMOTE_ADDR'
            );
            foreach ($keys as $key) {
                if (array_key_exists($key, $_SERVER) === true) {
                    foreach (explode(',', $_SERVER[$key]) as $ip) {
                        $ip = trim($ip);
                        $flag = $strict ? FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE : null;
                        if ($ip && filter_var($ip, FILTER_VALIDATE_IP, $flag) !== false) {
                            return $remote_addr = $ip;
                        }
                    }
                }
            }
            $remote_addr = null;
        }

        return $remote_addr;
    }

    /**
     * 编号加密
     *
     * @param $id
     * @return string
     */
    static function idEncrypt($id)
    {
        $pad = substr($id, -1, 1);
        $id = str_pad($id, 9, '0', STR_PAD_LEFT);
        $id = strtr(base64_encode($id), '+/', '-_');
        $id = substr($id, 0, 9) . $pad . substr($id, 9);
        return $id;
    }

    /**
     * 编号解密
     *
     * @param $id
     * @return int
     */
    static function idDecrypt($id)
    {
        if (strlen($id) < 12) return $id;
        $id = substr($id, 0, 9) . substr($id, 10);
        $id = base64_decode(strtr($id, '-_', '+/'));
        return (int)$id;
    }

    /**
     * 返回数据表标识
     *
     * @param $schemaTableName
     * @return string
     */
    static function parseTableName($schemaTableName)
    {
        $schemaInfo = explode('.', $schemaTableName);
        $tableName = explode(' ', $schemaInfo[1], 2);
        $tableName = $tableName[0];
        $tableNameInfo = explode('_', $tableName, 2);

        return preg_replace('#_\d{1,2}_\d$#', '', $tableNameInfo[1]);
    }

    /**
     * crc32循环冗余校验的方式计算非整型的分表ID后缀
     *
     * @param string $rule
     * @param int $bit
     * @return int
     */
    static function crc32($rule, $bit = 3)
    {
        $checksum = sprintf("%03u", crc32($rule));
        $suffix = substr($checksum, -3);

        return $suffix;
    }

    /**
     * 断言条件为真
     *
     * @param boolean $condition
     * @param string $message
     * @return void
     * @throw AssertException
     */
    static function assertTure($condition, $message)
    {
        if (!$condition) {
            throw new AssertException($message);
        }
    }

	static function assertTrue($condition, $message)
	{
		if (!$condition) {
			throw new AssertException($message);
		}
	}

    static function checkRefer()
    {
        if (empty($_GET['tt_from'])) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    static function autoVersion($file)
    {
        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $file)) {
            return 0;
        }
        $mtime = filemtime($_SERVER['DOCUMENT_ROOT'] . $file);
        return $mtime;
    }

    //格式化价格
    static function formatPrice($price)
    {
        $price /= 100;
        $price_array = array();
        if (is_numeric($price)) {
            $price_array = explode('.', $price);
            if (!empty($price_array[1]) && $price_array[1] === '00') {
                $format_price = $price_array[0];
            } else {
                $format_price = $price;
            }
        } else {
            $format_price = $price;
        }
        return $format_price;
    }

    static function urlSafeB64Encode($string)
    {
        $data = base64_encode($string);
        $data = str_replace(array('+', '/'), array('-', '_'), $data);
        return $data;
    }

    /**
     * json数据加密
     * @return string
     */
    static public function jsonEncryptc($txt)
    {
        $key = '7cc000945bfcb86f2b7c864c6ef87073';
        // 使用随机数发生器产生 0~32000 的值并 MD5()
        srand((double)microtime() * 1000000);
        $encrypt_key = md5(rand(0, 32000));

        // 变量初始化
        $ctr = 0;
        $tmp = '';

        // for 循环，$i 为从 0 开始，到小于 $txt 字串长度的整数
        for ($i = 0; $i < strlen($txt); $i++) {
            // 如果 $ctr = $encrypt_key 的长度，则 $ctr 清零
            $ctr = $ctr == strlen($encrypt_key) ? 0 : $ctr;
            // $tmp 字串在末尾增加两位，其第一位内容为 $encrypt_key 的第 $ctr 位，
            // 第二位内容为 $txt 的第 $i 位与 $encrypt_key 的 $ctr 位取异或。然后 $ctr = $ctr + 1
            $tmp .= $encrypt_key[$ctr] . ($txt[$i] ^ $encrypt_key[$ctr++]);
        }

        // 返回结果，结果为 passport_key() 函数返回值的 base64 编码结果
        return base64_encode(self::jsonPassportKey($tmp, $key));
    }

    /**
     * json数据加密
     *
     *
     */
    static public function jsonPassportKey($txt, $encrypt_key)
    {
        // 将 $encrypt_key 赋为 $encrypt_key 经 md5() 后的值
        $encrypt_key = md5($encrypt_key);

        // 变量初始化
        $ctr = 0;
        $tmp = '';

        // for 循环，$i 为从 0 开始，到小于 $txt 字串长度的整数
        for ($i = 0; $i < strlen($txt); $i++) {
            // 如果 $ctr = $encrypt_key 的长度，则 $ctr 清零
            $ctr = $ctr == strlen($encrypt_key) ? 0 : $ctr;
            // $tmp 字串在末尾增加一位，其内容为 $txt 的第 $i 位，
            // 与 $encrypt_key 的第 $ctr + 1 位取异或。然后 $ctr = $ctr + 1
            $tmp .= $txt[$i] ^ $encrypt_key[$ctr++];
        }

        // 返回 $tmp 的值作为结果
        return $tmp;
    }

    /**
     * 数据解密
     * @access public
     * @param  $txt
     * @param  $key
     * @return void
     */
    static public function passport_decrypt($txt) {
        $key = '7cc000945bfcb86f2b7c864c6ef87073';
        $ceshi =  base64_decode($txt);
        $txt = self::jsonPassportKey(base64_decode($txt), $key);
        $tmp = '';
        for ($i = 0; $i < strlen($txt); $i++) {
            $j = $txt[$i];
            $k = $txt[++$i];
            $tmp .= $j ^ $k;
        }
        $tmp = urldecode($tmp);
        return $tmp;

    }

    /**
     * 将格式良好的XML字符串转换为标准的php数组
     *
     * @param string $xmlStr
     * @return array
     */
    static public function xmlStringToArray($xmlStr)
    {
        $node = new \SimpleXMLElement($xmlStr);
        $name = $node->getName();
        return array($name => self::simpleXMLRecursionToArray($node));
    }

    /**
     * 将格式良好的XML文件内容转换为标准的php数组
     *
     * @param $file
     * @return array
     */
    static public function xmlFileToArray($file)
    {
        $node = new \SimpleXMLElement($file, null, true);
        $name = $node->getName();
        return array($name => self::simpleXMLRecursionToArray($node));
    }

    /**
     * 将SimpleXMLElement对象转换为标准的php数组
     *
     * @param $node
     * @return array|string
     */
    static public function simpleXMLRecursionToArray($node)
    {
        $return = array();
        $root = array();
        $k = 0;

        foreach ($node as $name => $child) {
            if ($child->count()) {
                $root[$name] = self::SimpleXMLRecursionToArray($child);
            } else {
                $root[$k][$name] = (string)$child;
                foreach ($child->attributes() as $key => $value) {
                    $root[$k]['@attributes'][$key] = (string)$value;
                }
            }

            $k++;
        }

        foreach ($root as $k => $v) {
            if (count($v) == 1 && is_numeric($k)) {
                $return[array_keys($v)[0]] = array_values($v)[0];
            } else {
                $return[$k] = $v;
            }
        }

        return $return;
    }

    /**
     * 获取数字数组的字符串
     * 以逗号分隔
     * 如果元素不为数字则抛异常
     *
     * @param string $arrayOrString
     * @static
     * @access public
     * @return void
     */
    public static function mustGetStringOfNumbers($arrayOrString)
    {
        $items = $arrayOrString;
        if (!is_array($items)) {
            $items = explode(',', $arrayOrString);
        }

        $arr = array();

        foreach ($items as $i) {

            $is_null = TRUE;

            switch ($i) {
                case $i === 0:
                    $is_null = FALSE;
                    break;
                case $i === null:
                    $is_null = TRUE;
                    break;
                case $i === '':
                    $is_null = TRUE;
                    break;
                case $i === false:
                    $is_null = TRUE;
                    break;
                default:
                    $is_null = FALSE;
            }
            if (!$is_null) {
                $number = trim($i);
                self::assertTure(is_numeric($number), "元素{$number}不是数字类型");
                $arr[] = $number;
            }
        }

        return implode(',', $arr);
    }
    
    /**
     * 获取csv文件的内容 
     * 
     * @param string $file 
     * @access private
     * @return array(array())
     */
    public static function getLinesFromCsvFile($file)
    {
        $arrs = array();
        $content = file_get_contents($file);
        $encoding = mb_detect_encoding($content, array('GBK', 'UTF-8'));
        if('UTF-8' != $encoding)
        {
            $content = mb_convert_encoding($content, 'UTF-8', $encoding);
        }

        foreach(explode("\n", $content) as $line)
        {
            $arr = str_getcsv($line);
            foreach($arr as &$item)
            {
                $item = trim($item);
            }

            $arrs[] = $arr;
        }

        return $arrs;
    }
}
 


