<?php

/**
 *
 *
 * @author
 * @copyright Beijing LuboTiandi Technology Co.,Ltd.
 */

namespace Core;

use General\Protocol\Http\Client;
use Yaf\Application;
use Yaf\Controller_Abstract;
use General\Util\Sender\Http as SenderHttp;
use General\Util\Output\JsonOutput;
use Core\Common;
use Exception;
use Yaf\Registry;

/**
 *
 * Class Basic
 * @package Core
 */
abstract class Basic extends Controller_Abstract {

    /**
     * 用户信息
     * @var {Array}
     */
    protected $_user = array();

    /**
     * 当前请求的url
     *
     * @var string $currentUrl
     */
    public $currentUrl;

    /**
     * 当前请求的controller
     *
     * @var string $currentController
     */
    public $currentController;

    /**
     * 要页面缓存的controller（见配置文件）
     *
     * @var string $cacheController
     */
    public $cacheController = array();

    public function init() {
        $this->currentUrl = $this->getRequest()->getServer('HTTP_HOST') . $this->getRequest()->getServer('REQUEST_URI');
        $this->currentController = $this->getRequest()->controller;
        $this->setHeader();
        // 判断如果为网站前台的请求，则验证跨站伪请求
        if (APP_NAME == 'Webapp' && \Core\Factory::cookie()->get("user_id")) {
            if (!$this->checkRequestCsrfIsValid()) {
                // 跨站伪请求
                $e = new \Exception('跨站伪请求');
                $this->logException('error', $e);
                exit(0);
            }
        }
        $this->setActionName();
    }


    public function setActionName()
    {
        $this->assign('controller', $this->getRequest()->getControllerName());
    }


    /**
     * 标准响应输出
     *
     * @access protected
     * @param $response string 响应正文
     * @param $format string 响应输出数据格式
     * @param $code int 返回的http状态码
     * @return void
     */
    public function sendHttpOutput($response, $format = 'json', $code = 200) {
        if (is_array($format)) {
            $output_format = $format[1];
            $format = $format[0];
        }
        switch ($format) {
            case 'json':
                $content = new JsonOutput($response);
                break;
            case 'jsonp':
                $content = new JsonOutput($response);
                $content->setCallback($this->_request->getQuery('callback'));
                break;
            default:
                $content = new JsonOutput($response);
        }
        $sender = new SenderHttp();
        $sender->setStatus($code);
        $content($sender);
    }

    public function sendErrorResponse($message)
    {
        $response = array(
            'errorNo'  => 10000,
            'errorMsg' => $message
        );

        $this->sendHttpOutput($response, 'json');
    }

    /**
     * 返回csrf token
     *
     * @return string
     */
    public function getRequestCsrfToken() {
        return Common::getSessionCsrfToken();
    }

    /**
     * 返回csrf token name
     *
     * @return string|\Yaf\Config_Abstract
     */
    public function getRequestCsrfName() {
        return Common::getSessionCsrfName();
    }

    /**
     * 验证请求
     *
     * @return bool
     */
    public function checkRequestCsrfIsValid() {
        if ($this->_request->isPost()
            || ($this->_request->isXmlHttpRequest() && $this->_request->method == 'POST')) {
            $name = self::getRequestCsrfName();
            $token = $this->_request->getPost($name);
            unset($_POST[$name]);
            if (!Common::checkSessionCsrfIsValid($token)) {
                return false;
            }

            return true;
        }

        return true;
    }

    /**
     * 根据当前使用的route协议生成URL
     *
     * @param $route
     * @param array $params
     * @return string
     * @throws \Exception\RuntimeException
     */
    public function createUrl($route, $params = array()) {
        return Common::createUrl($route, $params);
    }

    /**
     * 记录日志
     *
     * @param string $level 日志级别
     * @param string $message 日志消息如：'订单号{order.num}，发货失败'
     * @param array $content 替换内容如：array('{order.num}' => '18373207273')
     *
     * @return \General\Psr\Log\AbstractLogger
     */
    public function log($level, $message, $content = array()) {
        /**
         * @var $logger \General\Psr\Log\AbstractLogger
         */
        $logger = \Yaf\Registry::get('Mount')->get('Logger');
        return $logger->log($level, "REMOTE:" . \Core\Functions::remoteAddr() . ", " . $message, $content);
    }

    /**
     * 设置报头
     */
    public function setHeader() {
        header("Expires: Mon, 26 Jul 1970 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
        header("Cache-Control: private, must-revalidate, max-age=0, no-store, no-cache, post-check=0, pre-check=0");
        header("Pragma: no-cache");
        header("Content-type: text/html; charset=utf-8");
        header("Access-Control-Allow-Origin:*");
    }

    public function errorPage($message) {
        echo $message;
        exit;
    }

    /**
     * 传递变量到模板并考虑缓存，使用方法和$this->_view->assign相同
     *
     * @param string | array $name
     * @param mixed $value
     */
    public function assign($name, $value = null) {
        if (is_array($name)) {
            foreach ($name as $valName => $val) {
                $this->_view->assign($valName, $val);
            }
        } else {
            $this->_view->assign($name, $value);
        }
    }

    /*
     * 判断是否登陆
     */

    protected function checkLogin() {
//        \Core\Factory::session()->del('email');
//        $email = \Core\Factory::session()->get('email');
        $uid = \Core\Factory::session()->get('uid');

        if (empty($uid) && \Core\Functions::checkRefer()) {
            $backUri = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->redirect('/account/login?back_uri=' . urlencode($backUri));
            exit;
        }
    }

    /*
     * 判断是否强制登陆
     */
    protected function buyCheckLogin() {
        $login        = \Core\Factory::session()->get('order_login');
        $userId       = \Core\Factory::session()->get('shop_user_id');
        $cookieUserId = \Core\Factory::cookie()->get('user_id');
        if (empty($userId) || empty($cookieUserId) || $userId != $cookieUserId || !$login) {
            $backUri = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            if($this->isInToutiao()){
                $this->redirect('/toutiaologin/buyCheck?urlHost=' . urlencode($backUri));
                exit;
            }else{
                $this->redirect('/toutiaologin/webLogin?urlHost=' . urlencode($backUri));
                exit;
            }
        }

        return true;
    }

    /**
     * 设置 HTTP Status Header
     *
     * @access public
     * @param  int the status code
     * @param  string
     * @return void
     */
    function setStatusHeader($code = 200, $text = '') {
        $state = array(
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported'
        );

        if (isset($state[$code]) AND $text == '') {
            $text = $state[$code];
        }
        $server_protocol = (isset($_SERVER['SERVER_PROTOCOL'])) ? $_SERVER['SERVER_PROTOCOL'] : FALSE;

        if (substr(php_sapi_name(), 0, 3) == 'cgi') {
            header("Status: {$code} {$text}", TRUE);
        } elseif ($server_protocol == 'HTTP/1.1' OR $server_protocol == 'HTTP/1.0') {
            header($server_protocol . " {$code} {$text}", TRUE, $code);
        } else {
            header("HTTP/1.1 {$code} {$text}", TRUE, $code);
        }
    }

    /**
     * 显示错误页面
     * @param  string $template
     * @param  string $statusCode
     * @param  array  $ext
     */
    function showError($template, $statusCode, $ext = null) {
        $this->setStatusHeader($statusCode);
        $template = $this->_view->render('error/' . $template . '.phtml', $ext);
        echo $template;
    }

    public function logException($level, Exception $e)
    {
        $code    = $e->getMessage();
        if (isset(Registry::get('errorEncode')[$code])) {
            $message = Registry::get('errorEncode')[$code];
        } else {
            $code    = $e->getCode();
            $message = $e->getMessage();
        }

        $msg =  $message . "：\r\n" . "Uri: "     . @$_SERVER['REQUEST_URI'] . "\r\n" .
                "File: "    . $e->getFile() . "\r\n" .
                "Line: "    . $e->getLine() . "\r\n" .
                "Code: "    . $code . "\r\n" .
                "Message: " . $message . "\r\n" .
                "Trace: "   . "\r\n" . $e->getTraceAsString() . "\r\n" .
                "REMOTE: "  . "\r\n" . \Core\Functions::remoteAddr() . "\r\n" .
                "Query: "   . var_export(Application::app()->getDispatcher()->getRequest()->getQuery(), true) . "\r\n" .
                "POST: "    . var_export(Application::app()->getDispatcher()->getRequest()->getPost(), true) . "\r\n" .
                "PARAMS: "  . var_export(Application::app()->getDispatcher()->getRequest()->getParams(), true) . "\r\n";

        $logger = \Yaf\Registry::get('Mount')->get('Logger');
        /**
         * @var $logger \General\Psr\Log\AbstractLogger
         */
        return $logger->log($level, $msg);
    }

    /**
     * 获取客户端ip 
     * 
     * @access public
     * @return string
     */
    public function getClientIp()
    {
        $ip = $this->getRequest()->getServer('REMOTE_ADDR');

        $str = $this->getRequest()->getServer('HTTP_X_FORWARDED_FOR');
        if(preg_match_all('/\d+\.\d+\.\d+\.\d+/', $str, $matches))
        {
            $ip = end($matches[0]);
        };

        return $ip;
    }


    public function getCityByIp($ip)
    {
        if(ini_get('yaf.environ') == 'develop') {
            $ip = '118.194.194.234';
        }
        $http = new Client();
        $data = array(
            'format' => 'json',
            'ip'     => $ip
        );
        $return = $http->get('http://int.dpool.sina.com.cn/iplookup/iplookup.php', $data);

        return json_decode($return, true);
    }

    /**
     * header变为下载文件 
     * 
     * @param string $fileName 
     * @access protected
     * @return void
     */
    protected function _downloadHeader($fileName) {
        $header = 'Content-type:text/csv;';
        if($this->_isWindowsClient())
        {
            $header .= ' charset=gbk;';
        }

        header($header);
        header('Content-Type: application/force-download');
        header('Content-Disposition: attachment; filename=' . $fileName);
        header('Expires:0');
        header('Pragma:public');
    }

}
