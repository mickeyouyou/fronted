<?php
/**
 *
 *
 * @author
 * @copyright Beijing Jinritemai Technology Co.,Ltd.
 */
namespace Core;

use General\Db\Adapter\Adapter;
use General\Db\Adapter\AdapterPool;
use General\Db\Table\Table;
use General\Db\Sql\Sql;
use General\Db\Sql\TableIdentifier;
use General\Cache\CachePool;
use General\Mail\Message;
use General\Mail\Transport\Smtp;
use General\Mail\Transport\SmtpOptions;
use General\Mime\Part;
use General\Session\Session;
use General\Util\Cookie;
use General\File\BeansDb;
use General\File\Qiniu\Qiniu;
use General\File\Folder;
use General\File\File;
use General\Transfer\TransferPool;
use Exception;
use General\Pheanstalk\Pheanstalk;

abstract class Factory
{
    /**
     * @var array
     */
    static private $_tables = array();

    /**
     * @var array
     */
    static private $_sqls = array();

    /**
     * @var null
     */
    static private $_gearman = NULL;

    /**
     * Cache单例，根据ini默认配置返回缓存实例
     *
     * @return \General\Cache\Storage\StorageInterface|null
     * @throws \Exception\RuntimeException
     */
    static function cache($cacheIdentifier = 'cache')
    {
        if (CachePool::has(CachePool::DEFAULT_STORAGE)) {
            return CachePool::get();
        }

        if (empty($options)) {
            $cache = \Yaf\Registry::get('Mount')->get('ConfigLoader', $cacheIdentifier);
            if (!$cache instanceof \Yaf\Config\Ini) {
                throw new Exception\RuntimeException('The Factory::cache need default cache config');
            }
            $options = $cache->toArray();
        }

        foreach ($options as $k => &$v) {
            if (empty($v))
                unset($options[$k]);
        }
        $storage = CachePool::factory(
            array('storage'=>$options['storage'], 'namespace'=>$options['namespace'], 'ttl'=>$options['ttl'])
        );
        CachePool::register($storage);
        CachePool::get()->setResource($options);
        CachePool::get()->getResource();
        return CachePool::get();
    }


    /**
     * Reids单例，根据ini或自定义配置返回实例
     *
     * @param array $config
     * @return \General\Cache\Storage\StorageInterface|null
     * @throws \Exception\RuntimeException
     */
    static function redis($config = array())
    {
        $redis = array();
        if (empty($config)) {
            $redis = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'redis');
        }

        if ($redis instanceof \Yaf\Config\Ini) {
            $options = $redis->toArray();
        } else if (!empty($config)) {
            $options = $config;
        } else {
            throw new Exception\RuntimeException('The Factory::redis need redis config in ini or config');
        }


        foreach ($options as $k => &$v) {
            if (empty($v))
                unset($options[$k]);
        }
        $storageName = implode('_', $options);
        if (CachePool::has($storageName)) {
            return CachePool::get($storageName);
        }

        $storage = CachePool::factory(
            array('storage'=>'redis', 'namespace'=>$options['namespace'], 'ttl'=>$options['ttl'])
        );
        CachePool::register($storage, $storageName);
        CachePool::get($storageName)->setResource($options);
        CachePool::get($storageName)->getResource();
        return CachePool::get($storageName);
    }

    /**
     * Session 单例
     *
     * @throws \Exception\RuntimeException
     */
    static function session()
    {
        if (!Session::sessionExists()) {
            $session = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'session');
            if (!$session instanceof \Yaf\Config\Ini) {
                throw new Exception\RuntimeException('The Factory::session need default session config');
            }

            $session = $session->toArray();
            foreach ($session as $k => &$v) {
                if (empty($v)) {
                    unset($session[$k]);
                }
                if ($k == 'save_path') {
                    $session[$k] = urldecode($session[$k]);
                }
            }
            Session::setOptions($session);
        }
        return \Yaf\Session::getInstance();
    }

    /**
     * Cookie单例
     *
     * @return Cookie
     * @throws \Exception\RuntimeException
     */
    static function cookie()
    {
        $cookie = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'cookie');
        if (!$cookie instanceof \Yaf\Config\Ini) {
            throw new Exception\RuntimeException('The Factory::cookie need default cookie config');
        }
        $cookie = $cookie->toArray();
        return Cookie::getInstance($cookie);
    }

    /**
     * Db
     * @param string $name
     * @param array $config
     * @return \General\Db\Adapter\AdapterInterface|null
     * @throws \Exception\RuntimeException
     */
    static function db($name = AdapterPool::DEFAULT_ADAPTER, $config = array())
    {
        if (AdapterPool::has($name)) {
            return AdapterPool::get($name);
        }

        while (1) {
            if (!empty($config)) {
                $options = $config;
                break;
            }

            $db =  \Yaf\Registry::get('Mount')->get('ConfigLoader', 'database');
            if ($db instanceof \Yaf\Config\Ini) {
                $options = $db->toArray();
                break;
            }
            throw new Exception\RuntimeException('The Factory::db need db config in ini or config');
        }

        if (!empty($options[$name])) {
            $adapter = new Adapter($options[$name]);
        } else {
            $adapter = new Adapter($options);
        }

        AdapterPool::register($adapter, $name);

        $cache = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'cache');
        if ($cache instanceof \Yaf\Config\Ini && defined('APP_METADATA')) {
            $metaCache = $cache->toArray();
            foreach ($metaCache as $k => &$v) {
                if (empty($v))
                    unset($metaCache[$k]);
            }

            $metaCache['namespace'] = APP_METADATA;
            $metaCache['ttl'] = 600;
            $cacheHandler = \General\Cache\CachePool::factory(
                array(
                    'storage'   => $metaCache['storage'],
                    'namespace' => $metaCache['namespace'],
                    'ttl'       => $metaCache['ttl']
                )
            );
            \General\Cache\CachePool::register($cacheHandler, 'MetadataCacher');
            \General\Cache\CachePool::get('MetadataCacher')->setResource($metaCache);
        }

        return AdapterPool::get($name);
    }


    /**
     * 根据数据库适配器返回SQL对象
     *
     * @param null $table
     * @param Adapter $user_adapter
     * @return Sql
     * @throws \Exception\RuntimeException
     */
    static public function sql($table = null, Adapter $user_adapter = null)
    {
        $originalTable = Functions::parseTableName($table);

        $key = $table ?: ' ';
        if (isset(self::$_sqls[$key])) {
            return self::$_sqls[$key];
        } else {
            if (is_null($user_adapter)) {
                $mapper = \Yaf\Registry::get('Mount')->get('Mapper');
                if (!isset($mapper[$originalTable])) {
                    throw new Exception\RuntimeException(sprintf(
                        'The Factory::table %s Table Not Exist In DB Server Tables'
                    , $originalTable));
                } else {
                    $dbServer = $mapper[$originalTable];
                }
                $user_adapter = self::db($dbServer);
            }
            return self::$_sqls[$key] = new Sql($user_adapter, $table);
        }
    }

    /**
     * Table
     *
     * @param $table
     * @param Adapter $user_adapter
     * @return Table
     * @throws \Exception\RuntimeException
     */
    static public function table($table, Adapter $user_adapter = null)
    {
        $originalTable = Functions::parseTableName($table);

        $table = $table instanceof TableIdentifier ? $table : new TableIdentifier($table);
        $key = (string) $table;
        if (isset(self::$_tables[$key])) {
            return self::$_tables[$key];
        } else {
            if (is_null($user_adapter)) {
                $mapper = \Yaf\Registry::get('Mount')->get('Mapper');
                if (!isset($mapper[$originalTable])) {
                    throw new Exception\RuntimeException(sprintf(
                        'The Factory::table %s Table Not Exist In DB Server Tables'
                    , $originalTable));
                } else {
                    $dbServer = $mapper[$originalTable];
                }
                $user_adapter = self::db($dbServer);
            }
            return self::$_tables[$key] = new Table(array('table'=> $table, 'adapter'=>$user_adapter));
        }
    }

    /**
     * Row
     *
     * @param $table
     * @param $data
     * @param Adapter $user_adapter
     * @return \General\Db\Row\AbstractRow
     */
    static public function row($table, $data, Adapter $user_adapter = null)
    {
        return self::table($table, $user_adapter)->create($data);
    }

    /**
     * Beansdb
     *
     * @param array $config
     * @return BeansDb
     * @throws \Exception\RuntimeException
     */
    static public function beansdb($config = array())
    {
        $beansdb = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'beansdb');

        if ($beansdb instanceof \Yaf\Config\Ini && empty($config)) {
            $options = $beansdb->toArray();
        } elseif (!empty($config)) {
            $options = $config;
        } else {
            throw new Exception\RuntimeException('The Factory::beansdb need default beansdb config');
        }
        return new BeansDb($options);
    }

    /**
     * 七牛云储存
     *
     * @param array $config
     * @return Qiniu
     * @throws \Exception\RuntimeException
     */
    static public function qiniu($config = array())
    {
        $qiniu = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'qiniu');

        if ($qiniu instanceof \Yaf\Config\Ini && empty($config)) {
            $options = $qiniu->toArray();
        } else if (!empty($config)) {
            $options = $config;
        } else {
            throw new Exception\RuntimeException('The Factory::qiniu need default qiniu config');
        }

        if (!isset($options['accessKey'])) {
            $options['accessKey'] = $options['access_key'];
        }

        if (!isset($options['secretKey'])) {
            $options['secretKey'] = $options['secret_key'];
        }

        return new Qiniu($options);
    }

    /**
     * 普通文件夹操作
     *
     * @param array $config
     * @return Folder
     */
    static public function folder($config = array()) {
        return new Folder($config);
    }

    /**
     * 本地文件操作
     *
     * @param array $config
     * @return File
     */
    static public function normalFile($config = array()) {
        if (!isset($config['basePath'])) {
            $config['basePath'] = $config['base_path'];
        }

        return new File($config);
    }

    /**
     * 文件操作适配器
     *
     * @param array $config
     * @param null $storage
     * @return \General\File\StorageInterface
     */
    static public function file($storage = null, $config = array())
    {
        if (is_null($storage)) {
            $file = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'file');
        } else {
            $file = array();
        }

        if ($file instanceof \Yaf\Config\Ini && empty($config)) {
            $options = $file->toArray();
        } else if (!empty($config)) {
            $options = $config;
        } else {
            $options = array();
        }

        if (is_null($storage)) {
            $storage = $options['storage'];
        }

        return self::$storage($options);
    }

    /**
     * Gearman
     *
     * @return GearmanClient|null
     * @throws \Exception\RuntimeException
     */
    static public function gearman()
    {
        if (isset(self::$_gearman)) {
            return self::$_gearman;
        } else {
            $gearman = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'gearman');
            if (!$gearman instanceof \Yaf\Config\Ini) {
                throw new Exception\RuntimeException('The Factory::gearman need default gearman config');
            }
            $gearman = $gearman->toArray();
            self::$_gearman = new \GearmanClient();
            self::$_gearman->addServer($gearman['host'], $gearman['post']);
            return self::$_gearman;
        }
    }

    /**
     * 远程调用
     *
     * @param string $driver
     * @param array $config
     * @return \General\Transfer\Driver\DriverInterface|null
     * @throws \Exception\RuntimeException
     */
    static public function transfer($driver = 'YarClient', $config = array())
    {
        if (strpos($driver, 'Yar') !== false) {
            $yar = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'yar');

            if ($yar instanceof \Yaf\Config\Ini && empty($config)) {
                $options = $yar->toArray();
            } elseif (!empty($config)) {
                $options = $config;
            } else {
                throw new Exception\RuntimeException('The Factory::transfer need default yar config');
            }
        } else {
            $options = $config;
        }

        return TransferPool::factory($options, $driver);
    }

    /**
     * beanstalk客户端
     *
     * @param int $serverId
     * @param array $config
     * @return Pheanstalk
     * @throws \Exception\RuntimeException
     */
    static public function beanstalk($serverId = 0, $config = array())
    {
        $beanstalk = array();

        if (empty($config)) {
            $beanstalk = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'beanstalkd')->get('server');
        }

        if ($beanstalk instanceof \Yaf\Config\Ini) {
            $options = $beanstalk->toArray();
        } else if (!empty($config)) {
            $options = $config;
        } else {
            throw new Exception\RuntimeException('The Factory::beanstalk need default beanstalkd config');
        }

        if (isset($options[0])) {
            $selectServer = $options[$serverId];
        } else {
            $selectServer = $options;
        }
        return new Pheanstalk($selectServer['host'], $selectServer['port'], $selectServer['connect_timeout']);
    }

    /**
     * @return \General\Mail\Message
     */
    public static function mail()
    {

        return new Message();

    }

    /**
     * @param \General\Mail\Message $message
     */
    public static function mailSender(Message $message, $content)
    {
        $text = new Part();
        $text->type        = "text/html";
        $text->charset     = "utf-8";
        $text->description = "monitor data from mail.jinritemai.com";
        $text->boundary    = md5(time());
        $text->setContent('订单数据：' . var_export($content, true));

        $mimeMessage = new \General\Mime\Message();
        $mimeMessage->setParts(array($text));

        $message->setBody($mimeMessage);

        $transport = new Smtp();

        $mail = \Yaf\Registry::get('Mount')->get('ConfigLoader', 'mail');
        $options = new SmtpOptions(
            array(
                'name'              => $mail['name'],
                'host'              => $mail['host'],
                'port'              => $mail['port'],
                'connection_class'  => 'plain',
                'connection_config' => array(
                    'username' => $mail['username'],
                    'password' => $mail['password'],
                )
            )
        );

        $transport->setOptions($options);
        $transport->send($message);
    }
}