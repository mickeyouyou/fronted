<?php
/**
 *
 *
 *
 */
namespace Core;

use Yaf\Application;
use Yaf\Dispatcher;
use Yaf\Controller_Abstract;
use General\Crypt\AES;
use Exception as Exception;
use Yaf\Registry;

/**
 * Class Transfer
 *
 * @package Cloud\Core
 */
abstract class Api extends Controller_Abstract
{
    /**
     * @var string
     */
    protected $_output_format;

    /**
     * @var array
     */
    public $_params;

    /**
     * ServiceApi初始化
     */
    public function init()
    {
        Dispatcher::getInstance()->returnResponse(true);
        Dispatcher::getInstance()->disableView();
        $this->_params = $this->getRequest()->getParams();
        $this->checkToken();
    }

    /**
     * @param $response
     */
    public function sendOutput($response)
    {
        if (defined('YAR_DEBUG') && YAR_DEBUG === true) {
            \General\Debug\Debug::console($response);
        }

        if ($response !== null) {
            if (is_object($response) && method_exists($response, 'toArray')) {
                $response = $response->toArray();
            } elseif ($response instanceof \Traversable) {
                $temp = array();
                foreach ($response as $key => $val) {
                    $temp[$key] = $val;
                }
                $response = $temp;
            }
            $response = json_encode($response);
        }

        $this->getResponse()->setBody($response, 'content');
    }

    /**
     *
     *
     * @return bool
     * @throws \Exception
     */
    protected function checkToken()
    {
        do {
            // 调试模式
            if (defined('YAR_DEBUG') && YAR_DEBUG === true) {
                return true;
            }

            if (strtolower($this->getRequest()->getMethod()) != 'api') {
                $response = array(
                    'state' => false,
                    'error' => '非法请求',
                );
                break;
            }

            // 外部调用验证source->privileges
            if (isset($this->_params['yarsource']) && !empty($this->_params['yarsource'])) {
                return true;
            }

            // 内部调用验证本地token
            $aes = new AES();
            $aes->setKey(\Yaf\Registry::get("Mount")->get("ConfigLoader", 'yar')->secret);

            if (empty($this->_params['token'])) {
                $response = array(
                    'state' => false,
                    'error' => '请求令牌无效',
                );
                break;
            }

            $timestamp = $aes->decrypt(base64_decode($this->_params['token']));
            if ($timestamp && time() < $timestamp) {
                return true;
            } else {
                $response = array(
                    'state' => false,
                    'error' => '请求令牌失效',
                );
                break;
            }

        } while(0);

        if ($response && !$response['state']) {
            throw new Exception(json_encode(array('code'=>10, 'error'=>$response['error'])));
        }
    }

    /**
     * 记录日志
     *
     * @param string $level 日志级别
     * @param string $message 日志消息如：'订单号{order.num}，发货失败'
     * @param array $content 替换内容如：array('{order.num}' => '18373207273')
     *
     * @return \General\Psr\Log\AbstractLogger
     */
    public function log($level, $message, $content = array())
    {
        /**
         * @var $logger \General\Psr\Log\AbstractLogger
         */
        $logger = \Yaf\Registry::get('Mount')->get('Logger');
        return $logger->log($level, $message, $content);
    }

    public function logException($level, Exception $e)
    {
        $code    = $e->getMessage();
        if (isset(Registry::get('errorEncode')[$code])) {
            $message = Registry::get('errorEncode')[$code];
        } else {
            $code    = $e->getCode();
            $message = $e->getMessage();
        }

        $msg =  $message . "：\r\n" . "Uri: "     . @$_SERVER['REQUEST_URI'] . "\r\n" .
                "File: "    . $e->getFile() . "\r\n" .
                "Line: "    . $e->getLine() . "\r\n" .
                "Code: "    . $code . "\r\n" .
                "Message: " . $message . "\r\n" .
                "Trace: "   . "\r\n" . $e->getTraceAsString() . "\r\n" .
                "Query: "   . var_export(Application::app()->getDispatcher()->getRequest()->getQuery(), true) . "\r\n" .
                "POST: "    . var_export(Application::app()->getDispatcher()->getRequest()->getPost(), true) . "\r\n" .
                "PARAMS: "  . var_export(Application::app()->getDispatcher()->getRequest()->getParams(), true) . "\r\n";

        $logger = \Yaf\Registry::get('Mount')->get('Logger');
        /**
         * @var $logger \General\Psr\Log\AbstractLogger
         */
        return $logger->log($level, $msg);
    }
}