<?php
/**
 * @author fengzbao@qq.com
 * @copyright Copyright (c) fzb.me
 * @version $Id:1.0.0, Base.php, 2016-03-29 12:34 created (updated)$
 */

namespace Core;

abstract class BaseModel
{
    public $table;

    /**
     *
     *
     * @var 数据库处理句柄
     */
    public $handler;


    public function __construct ()
    {

    }

    public function select()
    {

    }

    public function add()
    {

    }


    public function update()
    {

    }

    /**
     * 做成全局的逻辑删除
     */
    public function delete()
    {

    }
}