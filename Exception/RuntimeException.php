<?php
/**
 *
 *
 * @copyright Copyright (c) 2013 Beijing Jinritemai Technology Co.,Ltd. (http://www.Jinritemai.com)
 */

namespace Exception;

class RuntimeException extends \RuntimeException implements ExceptionInterface
{
}
